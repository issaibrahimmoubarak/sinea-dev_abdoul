package ne.mha.sinea;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class SineaErrorController implements ErrorController {

	//Affichage de la page d'erreur
	@RequestMapping("/error")
	public String handleError(HttpServletRequest request,Model model) {
	    Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
	    
	    if (status != null) {
	        Integer statusCode = Integer.valueOf(status.toString());
	        //cas d'erreur 404
	        if(statusCode == HttpStatus.NOT_FOUND.value()) {
	            model.addAttribute("view_path", "error/error-404");
	            return Template.defaultTemplate;
	        }
	        //cas d'erreur 500
	        else if(statusCode == HttpStatus.INTERNAL_SERVER_ERROR.value()) {
	        	model.addAttribute("viewPath", "error/error-500");
	        	return Template.defaultTemplate2;
	        }
	    }
	    model.addAttribute("viewPath", "error/error");
	    return Template.defaultTemplate2;
	}

	public String getErrorPath() {
		// TODO Auto-generated method stub
		return null;
	}
	
	//Affichage de la page accès refusé
	@GetMapping("/AccessDenied")
	public String  addCommunes( Model model) {
		model.addAttribute("viewPath", "error/error-403");
		return Template.defaultTemplate2;
		
	}
}
