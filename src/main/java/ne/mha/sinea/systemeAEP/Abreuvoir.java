package ne.mha.sinea.systemeAEP;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import ne.mha.sinea.CommonProperties;

@Data
@Entity
@Table(name = "abreuvoir")
public class Abreuvoir extends CommonProperties{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "code")
	protected Integer code;
	@Column(name = "numero_abreuvoir")
	protected Integer numeroAbreuvoir;
	@Column(name = "code_ins")
	protected String codeINS;
	@Column(name = "nb_robinets")
	protected Integer nbRobinets;
	@Column(name = "nb_robinets_fonctionnels")
	protected Integer nbRobinetsFonctionnels;
	@Column(name = "latitude")
	protected Double latitude;
	@Column(name = "longitude")
	protected Double longitude;
	@JoinColumn(name = "code_systeme_aep", referencedColumnName = "code")
	@ManyToOne
	protected SystemeAEP systemeAEP;
}
