package ne.mha.sinea.systemeAEP;

//import java.sql.Date;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import ne.mha.sinea.CommonProperties;
import ne.mha.sinea.nomenclature.marquePompe.MarquePompe;
import ne.mha.sinea.nomenclature.modelePompe.ModelePompe;
import ne.mha.sinea.nomenclature.typeSystemeInstallation.TypeSystemeInstallation;
import ne.mha.sinea.nomenclature.typeSystemeProtection.TypeSystemeProtection;
import ne.mha.sinea.pem.Forage;

@Data
@Entity
@Table(name = "captage_systeme_aep")
public class CaptageSystemeAEP extends CommonProperties {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "code")
	protected Integer code;
	@JoinColumn(name = "code_type_systeme_protection", referencedColumnName = "code")
	@ManyToOne
	private TypeSystemeProtection typeSystemeProtection;
	
	@Column(name = "protection_tete_forage")
	private boolean protectionTeteForage;
	@Column(name = "etat_compteur_forage")
	private boolean etatCompteurForage;
	@Column(name = "index_compteur_forage")
	protected Double indexCompteurForage;
	@Column(name = "profondeur_totale_mesure")
	protected Double profondeurTotaleMesure;
	@Column(name = "profondeur_niveau_statique")
	protected Double profondeurNiveauStatique;
	@Column(name = "diametre_equipement")
	protected Double diametreEquipement;
	@JoinColumn(name = "code_type_systeme_installation", referencedColumnName = "code")
	@ManyToOne
	protected TypeSystemeInstallation typeSystemeInstallation;
	@Column(name = "puissance")
	protected Double puissance;
	@Column(name = "date_installation")
	private Date dateInstallation;
	@JoinColumn(name = "code_modele_pompe", referencedColumnName = "code")
	@ManyToOne
	private ModelePompe modelePompe;
	@JoinColumn(name = "code_marque_pompe", referencedColumnName = "code")
	@ManyToOne
	private MarquePompe marquePompe;
	@Column(name = "debit_nominale")
	protected Double debitNominal;
	@Column(name = "hmt")
	protected Double HMT;
	@Column(name = "longeur_colonne")
	protected Double longueurColonne;
	@Column(name = "type_colonne_exhaure")
	protected String typeColonneExhaure;
	@Column(name = "diametre_colonne")
	protected Double diametreColonne;
	@Column(name = "cote_installation_pompe")
	protected Double coteInstallationPompe;
	@Column(name = "tubage_protection_tete")
	private boolean tubageProtectionTete;
	@Column(name = "couvercle_protection_tete")
	private boolean couvercleProtectionTete;
	@Column(name = "possibilite_mesure_piezometrique")
	private boolean possibiliteMesurePiezometrique;
	@Column(name = "date_installation2")
	private Date dateInstallation2;
	@JoinColumn(name = "code_modele_pompe2", referencedColumnName = "code")
	@ManyToOne
	private ModelePompe modelePompe2; 
	@JoinColumn(name = "code_marque_pompe2", referencedColumnName = "code")
	@ManyToOne
	private MarquePompe marquePompe2; 
	@Column(name = "puissance2")
	protected Double puissance2; 
	@Column(name = "voltage")
	protected Double voltage;
	@Column(name = "type_tableau_distribution")
	protected String typeTableauDistribution; 
	@Column(name = "puissance_transformateur")
	protected Double puissanceTransformateur;
	@Column(name = "puissance_compteur_electrique")
	protected Double puissanceCompteurElectrique;
	@Column(name = "marqueModule")
	protected String marqueModule;
	@Column(name = "nombre_module")
	protected Integer nombreModule;
	@Column(name = "puissance_unitaire_module")
	protected Double puissanceUnitaireModule;
	@Column(name = "puissance_totale")
	protected Double puissanceTotale;
	@Column(name = "marque_onduleur")
	protected String marqueOnduleur;
	@Column(name = "puissance_onduleur")
	protected Double puissanceOnduleur;
	
	
	@JoinColumn(name = "code_systeme_aep", referencedColumnName = "code")
	@ManyToOne
	protected SystemeAEP systemeAEP;
	
	@JoinColumn(name = "code_forage", referencedColumnName = "code")
	@ManyToOne
	protected Forage forage;
}
