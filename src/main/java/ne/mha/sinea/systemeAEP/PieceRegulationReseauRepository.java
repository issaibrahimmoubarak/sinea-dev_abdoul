package ne.mha.sinea.systemeAEP;

import java.util.List;
import org.springframework.data.repository.CrudRepository;

public interface PieceRegulationReseauRepository extends CrudRepository<PieceRegulationReseau, Integer> {
	
	PieceRegulationReseau findByCode(Integer code);
	List<PieceRegulationReseau> findBySystemeAEP_NumeroIRH(String IRH);

}