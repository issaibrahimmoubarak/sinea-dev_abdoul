package ne.mha.sinea.nomenclature.milieu;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface MilieuRepository extends CrudRepository<Milieu, Integer> {
	Milieu findByCode(Integer code);
	Milieu findByIsDeletedFalseAndLibelle(String libelle);
	List<Milieu> findByIsDeletedFalseOrderByLibelleAsc();
	List<Milieu> findByIsDeletedFalse();
	List<Milieu> findByIsDeletedTrue();
	List<Milieu> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<Milieu> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
