package ne.mha.sinea.nomenclature.typePlanEau;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TypePlanEauForm {

	private int code;
	private String libelle;
	
}
