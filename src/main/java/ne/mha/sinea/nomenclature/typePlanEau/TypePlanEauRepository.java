package ne.mha.sinea.nomenclature.typePlanEau;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface TypePlanEauRepository extends CrudRepository<TypePlanEau, Integer> {
	TypePlanEau findByCode(Integer code);
	TypePlanEau findByIsDeletedFalseAndLibelle(String libelle);
	List<TypePlanEau> findByIsDeletedFalseOrderByLibelleAsc();
	List<TypePlanEau> findByIsDeletedFalse();
	List<TypePlanEau> findByIsDeletedTrue();
	List<TypePlanEau> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<TypePlanEau> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
