package ne.mha.sinea.nomenclature.etatPointEau;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface etatPointEauRepository extends CrudRepository<etatPointEau, Integer> {
	etatPointEau findByCode(Integer code);
	etatPointEau findByIsDeletedFalseAndLibelle(String libelle);
	List<etatPointEau> findByIsDeletedFalseOrderByLibelleAsc();
	List<etatPointEau> findByIsDeletedFalse();
	List<etatPointEau> findByIsDeletedTrue();
	List<etatPointEau> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<etatPointEau> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
