package ne.mha.sinea.nomenclature.typeExhaure;

import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class TypeExhaureRestController {

	@Autowired
	TypeExhaureRepository typeExhaureService;
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/typeExhaure")
	public int addTypeExhaure(@Validated TypeExhaureForm typeExhaureForm,BindingResult bindingResult, Model model) {
		TypeExhaure savedTypeExhaure = new TypeExhaure();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeExhaure P = new TypeExhaure();
					P.setLibelle(typeExhaureForm.getLibelle());
					savedTypeExhaure = typeExhaureService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeExhaure.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/updateTypeExhaure")
    public int updateTypeExhaure(@Validated TypeExhaureForm typeExhaureForm,BindingResult bindingResult, Model model) {
		TypeExhaure savedTypeExhaure = new TypeExhaure();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeExhaure P = typeExhaureService.findByCode(typeExhaureForm.getCode());
					P.setLibelle(typeExhaureForm.getLibelle());
					savedTypeExhaure = typeExhaureService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeExhaure.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/deleteTypeExhaure")
    public int deleteTypeExhaure(@Validated TypeExhaureForm typeExhaureForm,BindingResult bindingResult, Model model) {
		TypeExhaure savedTypeExhaure = new TypeExhaure();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeExhaure P = typeExhaureService.findByCode(typeExhaureForm.getCode());
					P.setIsDeleted(true);
					savedTypeExhaure = typeExhaureService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeExhaure.getCode();
		
        
    }
}
