package ne.mha.sinea.nomenclature.typeExhaure;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TypeExhaureForm {

	private int code;
	private String libelle;
	
}
