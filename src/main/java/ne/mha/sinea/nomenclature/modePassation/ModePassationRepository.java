package ne.mha.sinea.nomenclature.modePassation;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface ModePassationRepository extends CrudRepository<ModePassation, Integer> {
	ModePassation findByCode(Integer code);
	ModePassation findByIsDeletedFalseAndLibelle(String libelle);
	List<ModePassation> findByIsDeletedFalseOrderByLibelleAsc();
	List<ModePassation> findByIsDeletedFalse();
	List<ModePassation> findByIsDeletedTrue();
	List<ModePassation> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<ModePassation> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
