package ne.mha.sinea.nomenclature.typeTube;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;
@Controller
public class TypeTubeController {

	@Autowired
	TypeTubeRepository typeTubeService;
	
	//@PreAuthorize("hasAuthority('gestion des Type Tube')")
	@GetMapping("/typeTube")
	public String  addTypeTube(TypeTubeForm typeTubeForm, Model model) {
		try{
			List<TypeTube> typeTubes = typeTubeService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("typeTubes", typeTubes);
			model.addAttribute("viewPath", "nomenclature/typeTube/typeTube");
			
			}
		catch(Exception e){
				
			}
		
		return Template.nomenclatureTemplate;

	}
	
	
}
