package ne.mha.sinea.nomenclature.typeLieuPublic;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TypeLieuPublicForm {

	private int code;
	private String libelle;
	
}
