package ne.mha.sinea.nomenclature.typeOuvrage;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TypeOuvrageForm {

	private int code;
	private String libelle;
	
}
