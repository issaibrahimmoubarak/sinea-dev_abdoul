package ne.mha.sinea.nomenclature.typeOuvrage;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;
@Controller
public class TypeOuvrageController {

	@Autowired
	TypeOuvrageRepository typeOuvrageService;
	
	//@PreAuthorize("hasAuthority('gestion des Type Ouvrage')")
	@GetMapping("/typeOuvrage")
	public String  addTypeOuvrage(TypeOuvrageForm typeOuvrageForm, Model model) {
		try{
			List<TypeOuvrage> typeOuvrages = typeOuvrageService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("typeOuvrages", typeOuvrages);
			model.addAttribute("viewPath", "nomenclature/typeOuvrage/typeOuvrage");
			
			}
		catch(Exception e){
				
			}
		
		return Template.nomenclatureTemplate;

	}
	
	
}
