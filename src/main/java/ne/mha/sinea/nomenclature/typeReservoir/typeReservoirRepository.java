package ne.mha.sinea.nomenclature.typeReservoir;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface typeReservoirRepository extends CrudRepository<typeReservoir, Integer> {
	typeReservoir findByCode(Integer code);
	typeReservoir findByIsDeletedFalseAndLibelle(String libelle);
	List<typeReservoir> findByIsDeletedFalseOrderByLibelleAsc();
	List<typeReservoir> findByIsDeletedFalse();
	List<typeReservoir> findByIsDeletedTrue();
	List<typeReservoir> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<typeReservoir> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
