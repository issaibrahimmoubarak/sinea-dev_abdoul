package ne.mha.sinea.nomenclature.typeReservoir;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class typeReservoirForm {

	private int code;
	private String libelle;
	
}
