package ne.mha.sinea.nomenclature.typeReservoir;

import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class typeReservoirRestController {

	@Autowired
	typeReservoirRepository typeReservoirRepository;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/typeReservoir")
	public int addTypeReservoir(@Validated typeReservoirForm typeReservoirForm,BindingResult bindingResult, Model model) {
		typeReservoir savedTypeReservoir = new typeReservoir();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					typeReservoir P = new typeReservoir();
					P.setLibelle(typeReservoirForm.getLibelle());
					savedTypeReservoir = typeReservoirRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeReservoir.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/updateTypeReservoir")
    public int updateTypeReservoir(@Validated typeReservoirForm typeReservoirForm,BindingResult bindingResult, Model model) {
		typeReservoir savedTypeReservoir = new typeReservoir();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					typeReservoir P = typeReservoirRepository.findByCode(typeReservoirForm.getCode());
					P.setLibelle(typeReservoirForm.getLibelle());
					savedTypeReservoir = typeReservoirRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeReservoir.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/deleteTypeReservoir")
    public int deleteTypeReservoir(@Validated typeReservoirForm typeReservoirForm,BindingResult bindingResult, Model model) {
		typeReservoir savedTypeReservoir = new typeReservoir();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					typeReservoir P = typeReservoirRepository.findByCode(typeReservoirForm.getCode());
					P.setIsDeleted(true);
					savedTypeReservoir = typeReservoirRepository.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeReservoir.getCode();
		
        
    }
}
