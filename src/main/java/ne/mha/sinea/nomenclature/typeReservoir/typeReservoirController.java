package ne.mha.sinea.nomenclature.typeReservoir;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;

@Controller
public class typeReservoirController {

	@Autowired
	typeReservoirRepository typeReservoirRepository;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@GetMapping("/typeReservoir")
	public String  addTypeRéservoir(typeReservoirForm typeReservoirForm, Model model) {
		try{
			List<typeReservoir> typeReservoir = typeReservoirRepository.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("typeReservoir", typeReservoir);
			model.addAttribute("viewPath", "nomenclature/typeReservoir/typeReservoir");
			
			}
		catch(Exception e){
				
			}
		
		return Template.nomenclatureTemplate;

	}
	
	
}
