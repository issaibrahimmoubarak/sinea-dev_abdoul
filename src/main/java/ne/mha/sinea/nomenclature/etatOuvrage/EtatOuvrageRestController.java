package ne.mha.sinea.nomenclature.etatOuvrage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class EtatOuvrageRestController {

	@Autowired
	EtatOuvrageRepository etatOuvrageService;
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/etatOuvrage")
	public int addEtatOuvrage(@Validated EtatOuvrageForm etatOuvrageForm,BindingResult bindingResult, Model model) {
		EtatOuvrage savedEtatOuvrage = new EtatOuvrage();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					EtatOuvrage P = new EtatOuvrage();
					P.setLibelle(etatOuvrageForm.getLibelle());
					savedEtatOuvrage = etatOuvrageService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedEtatOuvrage.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/updateEtatOuvrage")
    public int updateEtatOuvrage(@Validated EtatOuvrageForm etatOuvrageForm,BindingResult bindingResult, Model model) {
		EtatOuvrage savedEtatOuvrage = new EtatOuvrage();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					EtatOuvrage P = etatOuvrageService.findByCode(etatOuvrageForm.getCode());
					P.setLibelle(etatOuvrageForm.getLibelle());
					savedEtatOuvrage = etatOuvrageService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedEtatOuvrage.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/deleteEtatOuvrage")
    public int deleteEtatOuvrage(@Validated EtatOuvrageForm etatOuvrageForm,BindingResult bindingResult, Model model) {
		EtatOuvrage savedEtatOuvrage = new EtatOuvrage();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					EtatOuvrage P = etatOuvrageService.findByCode(etatOuvrageForm.getCode());
					P.setIsDeleted(true);
					savedEtatOuvrage = etatOuvrageService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedEtatOuvrage.getCode();
		
        
    }
}
