package ne.mha.sinea.nomenclature.etatOuvrage;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface EtatOuvrageRepository extends CrudRepository<EtatOuvrage, Integer> {
	EtatOuvrage findByCode(Integer code);
	EtatOuvrage findByIsDeletedFalseAndLibelle(String libelle);
	List<EtatOuvrage> findByIsDeletedFalseOrderByLibelleAsc();
	List<EtatOuvrage> findByIsDeletedFalse();
	List<EtatOuvrage> findByIsDeletedTrue();
	List<EtatOuvrage> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<EtatOuvrage> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
