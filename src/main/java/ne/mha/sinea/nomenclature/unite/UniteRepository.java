package ne.mha.sinea.nomenclature.unite;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface UniteRepository extends CrudRepository<Unite, Integer> {
	Unite findByCode(Integer code);
	Unite findByIsDeletedFalseAndLibelle(String libelle);
	List<Unite> findByIsDeletedFalseOrderByLibelleAsc();
	List<Unite> findByIsDeletedFalse();
	List<Unite> findByIsDeletedTrue();
	List<Unite> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<Unite> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
