package ne.mha.sinea.nomenclature.unite;

import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class UniteRestController {

	@Autowired
	UniteRepository uniteService;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/unite")
	public int addUnite(@Validated UniteForm uniteForm,BindingResult bindingResult, Model model) {
		Unite savedUnite = new Unite();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					Unite P = new Unite();
					P.setLibelle(uniteForm.getLibelle());
					savedUnite = uniteService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedUnite.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/updateUnite")
    public int updateUnite(@Validated UniteForm uniteForm,BindingResult bindingResult, Model model) {
		Unite savedUnite = new Unite();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					Unite P = uniteService.findByCode(uniteForm.getCode());
					P.setLibelle(uniteForm.getLibelle());
					savedUnite = uniteService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedUnite.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/deleteUnite")
    public int deleteUnite(@Validated UniteForm uniteForm,BindingResult bindingResult, Model model) {
		Unite savedUnite = new Unite();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					Unite P = uniteService.findByCode(uniteForm.getCode());
					P.setIsDeleted(true);
					savedUnite = uniteService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedUnite.getCode();
		
        
    }
}
