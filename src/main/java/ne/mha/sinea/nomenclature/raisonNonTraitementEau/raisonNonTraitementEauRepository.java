package ne.mha.sinea.nomenclature.raisonNonTraitementEau;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface raisonNonTraitementEauRepository extends CrudRepository<raisonNonTraitementEau, Integer> {
	raisonNonTraitementEau findByCode(Integer code);
	raisonNonTraitementEau findByIsDeletedFalseAndLibelle(String libelle);
	List<raisonNonTraitementEau> findByIsDeletedFalseOrderByLibelleAsc();
	List<raisonNonTraitementEau> findByIsDeletedFalse();
	List<raisonNonTraitementEau> findByIsDeletedTrue();
	List<raisonNonTraitementEau> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<raisonNonTraitementEau> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
