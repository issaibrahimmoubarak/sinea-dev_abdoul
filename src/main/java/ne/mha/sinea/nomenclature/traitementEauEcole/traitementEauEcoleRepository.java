package ne.mha.sinea.nomenclature.traitementEauEcole;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface traitementEauEcoleRepository extends CrudRepository<traitementEauEcole, Integer> {
	traitementEauEcole findByCode(Integer code);
	traitementEauEcole findByIsDeletedFalseAndLibelle(String libelle);
	List<traitementEauEcole> findByIsDeletedFalseOrderByLibelleAsc();
	List<traitementEauEcole> findByIsDeletedFalse();
	List<traitementEauEcole> findByIsDeletedTrue();
	List<traitementEauEcole> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<traitementEauEcole> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
