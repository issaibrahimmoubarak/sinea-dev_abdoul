package ne.mha.sinea.nomenclature.typeAnalyse;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface typeAnalyseRepository extends CrudRepository<typeAnalyse, Integer> {
	typeAnalyse findByCode(Integer code);
	typeAnalyse findByIsDeletedFalseAndLibelle(String libelle);
	List<typeAnalyse> findByIsDeletedFalseOrderByLibelleAsc();
	List<typeAnalyse> findByIsDeletedFalse();
	List<typeAnalyse> findByIsDeletedTrue();
	List<typeAnalyse> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<typeAnalyse> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
