package ne.mha.sinea.nomenclature.natureSol;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class NatureSolForm {

	private int code;
	private String libelle;
	
}
