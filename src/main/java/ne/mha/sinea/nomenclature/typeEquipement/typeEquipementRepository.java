package ne.mha.sinea.nomenclature.typeEquipement;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface typeEquipementRepository extends CrudRepository<typeEquipement, Integer> {
	typeEquipement findByCode(Integer code);
	typeEquipement findByIsDeletedFalseAndLibelle(String libelle);
	List<typeEquipement> findByIsDeletedFalseOrderByLibelleAsc();
	List<typeEquipement> findByIsDeletedFalse();
	List<typeEquipement> findByIsDeletedTrue();
	List<typeEquipement> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<typeEquipement> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
