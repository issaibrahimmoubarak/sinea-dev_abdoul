package ne.mha.sinea.nomenclature.typeOuvrageAssainissement;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface typeOuvrageAssainissementRepository extends CrudRepository<typeOuvrageAssainissement, Integer> {
	typeOuvrageAssainissement findByCode(Integer code);
	typeOuvrageAssainissement findByIsDeletedFalseAndLibelle(String libelle);
	List<typeOuvrageAssainissement> findByIsDeletedFalseOrderByLibelleAsc();
	List<typeOuvrageAssainissement> findByIsDeletedFalse();
	List<typeOuvrageAssainissement> findByIsDeletedTrue();
	List<typeOuvrageAssainissement> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<typeOuvrageAssainissement> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
