package ne.mha.sinea.nomenclature.uniteMesure;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface uniteMesureRepository extends CrudRepository<uniteMesure, Integer> {
	uniteMesure findByCode(Integer code);
	uniteMesure findByIsDeletedFalseAndLibelle(String libelle);
	List<uniteMesure> findByIsDeletedFalseOrderByLibelleAsc();
	List<uniteMesure> findByIsDeletedFalse();
	List<uniteMesure> findByIsDeletedTrue();
	List<uniteMesure> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<uniteMesure> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
