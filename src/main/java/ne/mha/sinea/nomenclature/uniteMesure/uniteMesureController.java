package ne.mha.sinea.nomenclature.uniteMesure;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;

@Controller
public class uniteMesureController {

	@Autowired
	uniteMesureRepository uniteMesureService;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@GetMapping("/uniteMesure")
	public String  addUniteMesure( Model model) {
		try{
			List<uniteMesure> uniteMesures = uniteMesureService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("uniteMesures", uniteMesures);
			model.addAttribute("viewPath", "nomenclature/uniteMesure/uniteMesure");
			
			}
		catch(Exception e){
				
			}
		
		return Template.nomenclatureTemplate;

	}
	
	
}
