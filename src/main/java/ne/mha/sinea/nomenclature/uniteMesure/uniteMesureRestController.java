package ne.mha.sinea.nomenclature.uniteMesure;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class uniteMesureRestController {

	@Autowired
	uniteMesureRepository uniteMesureService;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/uniteMesure")
	public int addUniteMesure(@Validated uniteMesureForm uniteMesureForm,BindingResult bindingResult, Model model) {
		uniteMesure savedUniteMesure = new uniteMesure();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					uniteMesure P = new uniteMesure();
					P.setLibelle(uniteMesureForm.getLibelle());
					savedUniteMesure = uniteMesureService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedUniteMesure.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/updateUniteMesure")
    public int updateUniteMesure(@Validated uniteMesureForm uniteMesureForm,BindingResult bindingResult, Model model) {
		uniteMesure savedUniteMesure = new uniteMesure();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					uniteMesure P = uniteMesureService.findByCode(uniteMesureForm.getCode());
					P.setLibelle(uniteMesureForm.getLibelle());
					savedUniteMesure = uniteMesureService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedUniteMesure.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@PostMapping("/deleteUniteMesure")
    public int deleteUniteMesure(@Validated uniteMesureForm uniteMesureForm,BindingResult bindingResult, Model model) {
		uniteMesure savedUniteMesure = new uniteMesure();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					uniteMesure P = uniteMesureService.findByCode(uniteMesureForm.getCode());
					P.setIsDeleted(true);
					savedUniteMesure = uniteMesureService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedUniteMesure.getCode();
		
        
    }
}
