package ne.mha.sinea.nomenclature.typeMarches;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class TypeMarchesRestController {

	@Autowired
	TypeMarchesRepository typeMarchesService;
	//@PreAuthorize("hasAuthority('gestion des typeMarchess')")
	@PostMapping("/typeMarches")
	public int addz(@Validated TypeMarchesForm typeMarchesForm,BindingResult bindingResult, Model model) {
		TypeMarches savedTypeMarches = new TypeMarches();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeMarches P = new TypeMarches();
					P.setLibelle(typeMarchesForm.getLibelle());
					savedTypeMarches = typeMarchesService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeMarches.getCode();
		
        
    }
	@PreAuthorize("hasAuthority('gestion des typeMarchess')")
	@PostMapping("/updateTypeMarches")
    public int updateTypeMarches(@Validated TypeMarchesForm typeMarchesForm,BindingResult bindingResult, Model model) {
		TypeMarches savedTypeMarches = new TypeMarches();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeMarches P = typeMarchesService.findByCode(typeMarchesForm.getCode());
					P.setLibelle(typeMarchesForm.getLibelle());
					savedTypeMarches = typeMarchesService.save(P);
					
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeMarches.getCode();
		
        
    }
	@PreAuthorize("hasAuthority('gestion des typeMarchess')")
	@PostMapping("/deleteTypeMarches")
    public int deleteTypeMarches(@Validated TypeMarchesForm typeMarchesForm,BindingResult bindingResult, Model model) {
		TypeMarches savedTypeMarches = new TypeMarches();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeMarches P = typeMarchesService.findByCode(typeMarchesForm.getCode());
					P.setIsDeleted(true);
					savedTypeMarches = typeMarchesService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeMarches.getCode();
		
        
    }
}
