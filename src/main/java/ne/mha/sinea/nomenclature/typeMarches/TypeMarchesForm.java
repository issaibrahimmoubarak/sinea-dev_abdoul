package ne.mha.sinea.nomenclature.typeMarches;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TypeMarchesForm {

	private int code;
	private String libelle;
	
}
