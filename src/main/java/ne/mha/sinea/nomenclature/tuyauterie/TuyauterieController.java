package ne.mha.sinea.nomenclature.tuyauterie;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;

@Controller
public class TuyauterieController {

	@Autowired
	TuyauterieRepository tuyauterieService;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@GetMapping("/tuyauterie")
	public String  addTuyauterie(TuyauterieForm tuyauterieForm, Model model) {
		try{
			List<Tuyauterie> tuyauteries = tuyauterieService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("tuyauteries", tuyauteries);
			model.addAttribute("viewPath", "nomenclature/tuyauterie/tuyauterie");
			
			}
		catch(Exception e){
				
			}
		
		return Template.nomenclatureTemplate;

	}
	
	
}
