package ne.mha.sinea.nomenclature.marqueOnduleur;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface marqueOnduleurRepository extends CrudRepository<marqueOnduleur, Integer> {
	marqueOnduleur findByCode(Integer code);
	marqueOnduleur findByIsDeletedFalseAndLibelle(String libelle);
	List<marqueOnduleur> findByIsDeletedFalseOrderByLibelleAsc();
	List<marqueOnduleur> findByIsDeletedFalse();
	List<marqueOnduleur> findByIsDeletedTrue();
	List<marqueOnduleur> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<marqueOnduleur> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
