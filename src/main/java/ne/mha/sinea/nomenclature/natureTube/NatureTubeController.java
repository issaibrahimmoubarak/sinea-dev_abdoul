package ne.mha.sinea.nomenclature.natureTube;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;
@Controller
public class NatureTubeController {

	@Autowired
	NatureTubeRepository natureTubeService;
	
	//@PreAuthorize("hasAuthority('gestion des types ressources')")
	@GetMapping("/natureTube")
	public String  addNatureTube(NatureTubeForm natureTubeForm, Model model) {
		try{
			List<NatureTube> natureTubes = natureTubeService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("natureTubes", natureTubes);
			model.addAttribute("viewPath", "nomenclature/natureTube/natureTube");
			
			}
		catch(Exception e){
				
			}
		
		return Template.nomenclatureTemplate;

	}
	
	
}
