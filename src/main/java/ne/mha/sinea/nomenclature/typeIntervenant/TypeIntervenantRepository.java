package ne.mha.sinea.nomenclature.typeIntervenant;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface TypeIntervenantRepository extends CrudRepository<TypeIntervenant, Integer> {
	TypeIntervenant findByCode(Integer code);
	TypeIntervenant findByIsDeletedFalseAndLibelle(String libelle);
	List<TypeIntervenant> findByIsDeletedFalseOrderByLibelleAsc();
	List<TypeIntervenant> findByIsDeletedFalse();
	List<TypeIntervenant> findByIsDeletedTrue();
	List<TypeIntervenant> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<TypeIntervenant> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
