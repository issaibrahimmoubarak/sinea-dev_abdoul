package ne.mha.sinea.nomenclature.typeStationMeteo;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface TypeStationMeteoRepository extends CrudRepository<TypeStationMeteo, Integer> {
	TypeStationMeteo findByCode(Integer code);
	TypeStationMeteo findByIsDeletedFalseAndLibelle(String libelle);
	List<TypeStationMeteo> findByIsDeletedFalseOrderByLibelleAsc();
	List<TypeStationMeteo> findByIsDeletedFalse();
	List<TypeStationMeteo> findByIsDeletedTrue();
	List<TypeStationMeteo> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<TypeStationMeteo> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
