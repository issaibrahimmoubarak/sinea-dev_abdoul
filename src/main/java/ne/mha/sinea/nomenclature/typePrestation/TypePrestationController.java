package ne.mha.sinea.nomenclature.typePrestation;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;
@Controller
public class TypePrestationController {

	@Autowired
	TypePrestationRepository typePrestationService;
	
	//@PreAuthorize("hasAuthority('gestion des Type Prestation')")
	@GetMapping("/typePrestation")
	public String  addTypePrestation(TypePrestationForm typePrestationForm, Model model) {
		try{
			List<TypePrestation> typePrestations = typePrestationService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("typePrestations", typePrestations);
			model.addAttribute("viewPath", "nomenclature/typePrestation/typePrestation");
			
			}
		catch(Exception e){
				
			}
		
		return Template.nomenclatureTemplate;

	}
	
	
}
