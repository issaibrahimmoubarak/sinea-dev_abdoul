package ne.mha.sinea.nomenclature.quantiteEauAep;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class quantiteEauAepForm {

	private int code;
	private String libelle;
	
}
