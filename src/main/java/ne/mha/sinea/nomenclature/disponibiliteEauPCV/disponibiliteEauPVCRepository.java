package ne.mha.sinea.nomenclature.disponibiliteEauPCV;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface disponibiliteEauPVCRepository extends CrudRepository<disponibiliteEauPVC, Integer> {
	disponibiliteEauPVC findByCode(Integer code);
	disponibiliteEauPVC findByIsDeletedFalseAndLibelle(String libelle);
	List<disponibiliteEauPVC> findByIsDeletedFalseOrderByLibelleAsc();
	List<disponibiliteEauPVC> findByIsDeletedFalse();
	List<disponibiliteEauPVC> findByIsDeletedTrue();
	List<disponibiliteEauPVC> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<disponibiliteEauPVC> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
