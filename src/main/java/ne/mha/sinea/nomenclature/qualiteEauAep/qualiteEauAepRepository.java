package ne.mha.sinea.nomenclature.qualiteEauAep;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface qualiteEauAepRepository extends CrudRepository<qualiteEauAep, Integer> {
	qualiteEauAep findByCode(Integer code);
	qualiteEauAep findByIsDeletedFalseAndLibelle(String libelle);
	List<qualiteEauAep> findByIsDeletedFalseOrderByLibelleAsc();
	List<qualiteEauAep> findByIsDeletedFalse();
	List<qualiteEauAep> findByIsDeletedTrue();
	List<qualiteEauAep> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<qualiteEauAep> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
