package ne.mha.sinea.nomenclature.typeTableauDeDistributionMiniAEP;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface typeTableauDeDistributionMiniAEPRepository extends CrudRepository<typeTableauDeDistributionMiniAEP, Integer> {
	typeTableauDeDistributionMiniAEP findByCode(Integer code);
	typeTableauDeDistributionMiniAEP findByIsDeletedFalseAndLibelle(String libelle);
	List<typeTableauDeDistributionMiniAEP> findByIsDeletedFalseOrderByLibelleAsc();
	List<typeTableauDeDistributionMiniAEP> findByIsDeletedFalse();
	List<typeTableauDeDistributionMiniAEP> findByIsDeletedTrue();
	List<typeTableauDeDistributionMiniAEP> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<typeTableauDeDistributionMiniAEP> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
