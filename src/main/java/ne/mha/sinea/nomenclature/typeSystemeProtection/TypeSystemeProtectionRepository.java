package ne.mha.sinea.nomenclature.typeSystemeProtection;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface TypeSystemeProtectionRepository extends CrudRepository<TypeSystemeProtection, Integer> {
	TypeSystemeProtection findByCode(Integer code);
	TypeSystemeProtection findByIsDeletedFalseAndLibelle(String libelle);
	List<TypeSystemeProtection> findByIsDeletedFalseOrderByLibelleAsc();
	List<TypeSystemeProtection> findByIsDeletedFalse();
	List<TypeSystemeProtection> findByIsDeletedTrue();
	List<TypeSystemeProtection> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<TypeSystemeProtection> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
