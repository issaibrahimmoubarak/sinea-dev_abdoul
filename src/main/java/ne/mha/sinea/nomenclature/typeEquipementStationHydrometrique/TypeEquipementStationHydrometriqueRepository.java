package ne.mha.sinea.nomenclature.typeEquipementStationHydrometrique;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface TypeEquipementStationHydrometriqueRepository extends CrudRepository<TypeEquipementStationHydrometrique, Integer> {
	TypeEquipementStationHydrometrique findByCode(Integer code);
	TypeEquipementStationHydrometrique findByIsDeletedFalseAndLibelle(String libelle);
	List<TypeEquipementStationHydrometrique> findByIsDeletedFalseOrderByLibelleAsc();
	List<TypeEquipementStationHydrometrique> findByIsDeletedFalse();
	List<TypeEquipementStationHydrometrique> findByIsDeletedTrue();
	List<TypeEquipementStationHydrometrique> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<TypeEquipementStationHydrometrique> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
