package ne.mha.sinea.nomenclature.typeEquipementStationHydrometrique;

import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class TypeEquipementStationHydrometriqueRestController {

	@Autowired
	TypeEquipementStationHydrometriqueRepository typeEquipementStationHydrometriqueService;
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/typeEquipementStationHydrometrique")
	public int addTypeEquipementStationHydrometrique(@Validated TypeEquipementStationHydrometriqueForm typeEquipementStationHydrometriqueForm,BindingResult bindingResult, Model model) {
		TypeEquipementStationHydrometrique savedTypeEquipementStationHydrometrique = new TypeEquipementStationHydrometrique();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeEquipementStationHydrometrique P = new TypeEquipementStationHydrometrique();
					P.setLibelle(typeEquipementStationHydrometriqueForm.getLibelle());
					savedTypeEquipementStationHydrometrique = typeEquipementStationHydrometriqueService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeEquipementStationHydrometrique.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/updateTypeEquipementStationHydrometrique")
    public int updateTypeEquipementStationHydrometrique(@Validated TypeEquipementStationHydrometriqueForm typeEquipementStationHydrometriqueForm,BindingResult bindingResult, Model model) {
		TypeEquipementStationHydrometrique savedTypeEquipementStationHydrometrique = new TypeEquipementStationHydrometrique();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeEquipementStationHydrometrique P = typeEquipementStationHydrometriqueService.findByCode(typeEquipementStationHydrometriqueForm.getCode());
					P.setLibelle(typeEquipementStationHydrometriqueForm.getLibelle());
					savedTypeEquipementStationHydrometrique = typeEquipementStationHydrometriqueService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeEquipementStationHydrometrique.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/deleteTypeEquipementStationHydrometrique")
    public int deleteTypeEquipementStationHydrometrique(@Validated TypeEquipementStationHydrometriqueForm typeEquipementStationHydrometriqueForm,BindingResult bindingResult, Model model) {
		TypeEquipementStationHydrometrique savedTypeEquipementStationHydrometrique = new TypeEquipementStationHydrometrique();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					TypeEquipementStationHydrometrique P = typeEquipementStationHydrometriqueService.findByCode(typeEquipementStationHydrometriqueForm.getCode());
					P.setIsDeleted(true);
					savedTypeEquipementStationHydrometrique = typeEquipementStationHydrometriqueService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedTypeEquipementStationHydrometrique.getCode();
		
        
    }
}
