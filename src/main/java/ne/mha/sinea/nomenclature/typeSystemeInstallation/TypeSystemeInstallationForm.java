package ne.mha.sinea.nomenclature.typeSystemeInstallation;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TypeSystemeInstallationForm {

	private int code;
	private String libelle;
	
}
