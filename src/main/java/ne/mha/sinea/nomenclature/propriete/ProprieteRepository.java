package ne.mha.sinea.nomenclature.propriete;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface ProprieteRepository extends CrudRepository<Propriete, Integer> {
	Propriete findByCode(Integer code);
	Propriete findByIsDeletedFalseAndLibelle(String libelle);
	List<Propriete> findByIsDeletedFalseOrderByLibelleAsc();
	List<Propriete> findByIsDeletedFalse();
	List<Propriete> findByIsDeletedTrue();
	List<Propriete> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<Propriete> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
