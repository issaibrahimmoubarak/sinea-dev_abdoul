package ne.mha.sinea.nomenclature.parametreStationHydrometrique;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;
@Controller
public class ParametreStationHydrometriqueController {

	@Autowired
	ParametreStationHydrometriqueRepository parametreStationHydrometriqueService;
	
	//@PreAuthorize("hasAuthority('gestion des types ressources')")
	@GetMapping("/parametreStationHydrometrique")
	public String  addParametreStationHydrometrique(ParametreStationHydrometriqueForm parametreStationHydrometriqueForm, Model model) {
		try{
			List<ParametreStationHydrometrique> parametreStationHydrometriques = parametreStationHydrometriqueService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("parametreStationHydrometriques", parametreStationHydrometriques);
			model.addAttribute("viewPath", "nomenclature/parametreStationHydrometrique/parametreStationHydrometrique");
			
			}
		catch(Exception e){
				
			}
		
		return Template.nomenclatureTemplate;

	}
	
	
}
