package ne.mha.sinea.nomenclature.typeReseau;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface typeReseauRepository extends CrudRepository<typeReseau, Integer> {
	typeReseau findByCode(Integer code);
	typeReseau findByIsDeletedFalseAndLibelle(String libelle);
	List<typeReseau> findByIsDeletedFalseOrderByLibelleAsc();
	List<typeReseau> findByIsDeletedFalse();
	List<typeReseau> findByIsDeletedTrue();
	List<typeReseau> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<typeReseau> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
