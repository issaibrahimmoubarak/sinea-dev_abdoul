package ne.mha.sinea.nomenclature.parametreStationMeteo;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;
@Controller
public class ParametreStationMeteoController {

	@Autowired
	ParametreStationMeteoRepository parametreStationMeteoService;
	
	//@PreAuthorize("hasAuthority('gestion des types ressources')")
	@GetMapping("/parametreStationMeteo")
	public String  addParametreStationMeteo(ParametreStationMeteoForm parametreStationMeteoForm, Model model) {
		try{
			List<ParametreStationMeteo> parametreStationMeteos = parametreStationMeteoService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("parametreStationMeteos", parametreStationMeteos);
			model.addAttribute("viewPath", "nomenclature/parametreStationMeteo/parametreStationMeteo");
			
			}
		catch(Exception e){
				
			}
		
		return Template.nomenclatureTemplate;

	}
	
	
}
