package ne.mha.sinea.nomenclature.intervenant;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface IntervenantRepository extends CrudRepository<Intervenant, Integer> {
	Intervenant findByCode(Integer code);
	Intervenant findByDenomination(String denomination);
	List<Intervenant> findByTypeIntervenant_Libelle(String libelle);
	List<Intervenant> findByTypeIntervenant_Code(Integer code);
	List<Intervenant> findByIsDeletedFalse();
	List<Intervenant> findByIsDeletedTrue();

}
