package ne.mha.sinea.nomenclature.intervenant;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.ResponseEntity;
//import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
// import org.springframework.validation.BindingResult;
// import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
// import org.springframework.web.bind.annotation.PathVariable;
// import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.multipart.MultipartFile;
// import org.springframework.web.servlet.mvc.support.RedirectAttributes;
// import org.springframework.web.servlet.view.RedirectView;
import ne.mha.sinea.Template;
import ne.mha.sinea.nomenclature.typeIntervenant.TypeIntervenant;
import ne.mha.sinea.nomenclature.typeIntervenant.TypeIntervenantRepository;

@Controller
public class IntervenantController {

	
		@Autowired
	TypeIntervenantRepository typeIntervenantService;

	// formulaire intervenant dont l'ajout , l'import et l'export
	// @PreAuthorize("hasAuthority('Ajout Intervenant'))
	@GetMapping("/intervenant")
	public String addIntervenant(IntervenantForm intervenantForm, Model model) {
		try {
			List<TypeIntervenant> typeIntervenants = typeIntervenantService.findByIsDeletedFalse();
			model.addAttribute("typeIntervenants", typeIntervenants);
			model.addAttribute("viewPath", "nomenclature/intervenant/intervenant");
			
		} catch (Exception e) {

		}
		return Template.nomenclatureTemplate;
	}

}
