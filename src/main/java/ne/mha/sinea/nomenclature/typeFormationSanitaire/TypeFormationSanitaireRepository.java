package ne.mha.sinea.nomenclature.typeFormationSanitaire;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface TypeFormationSanitaireRepository extends CrudRepository<TypeFormationSanitaire, Integer> {
	TypeFormationSanitaire findByCode(Integer code);
	TypeFormationSanitaire findByIsDeletedFalseAndLibelle(String libelle);
	List<TypeFormationSanitaire> findByIsDeletedFalseOrderByLibelleAsc();
	List<TypeFormationSanitaire> findByIsDeletedFalse();
	List<TypeFormationSanitaire> findByIsDeletedTrue();
	List<TypeFormationSanitaire> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<TypeFormationSanitaire> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
