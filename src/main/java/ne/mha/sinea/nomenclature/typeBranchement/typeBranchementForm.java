package ne.mha.sinea.nomenclature.typeBranchement;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class typeBranchementForm {

	private int code;
	private String libelle;
	
}
