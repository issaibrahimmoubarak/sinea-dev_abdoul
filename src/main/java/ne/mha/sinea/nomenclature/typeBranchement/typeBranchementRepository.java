package ne.mha.sinea.nomenclature.typeBranchement;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface typeBranchementRepository extends CrudRepository<typeBranchement, Integer> {
	typeBranchement findByCode(Integer code);
	typeBranchement findByIsDeletedFalseAndLibelle(String libelle);
	List<typeBranchement> findByIsDeletedFalseOrderByLibelleAsc();
	List<typeBranchement> findByIsDeletedFalse();
	List<typeBranchement> findByIsDeletedTrue();
	List<typeBranchement> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<typeBranchement> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
