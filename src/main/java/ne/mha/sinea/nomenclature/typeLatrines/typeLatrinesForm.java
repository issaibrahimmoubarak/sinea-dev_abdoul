package ne.mha.sinea.nomenclature.typeLatrines;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class typeLatrinesForm {

	private int code;
	private String libelle;
	
}
