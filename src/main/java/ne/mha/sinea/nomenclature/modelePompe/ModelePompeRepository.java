package ne.mha.sinea.nomenclature.modelePompe;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface ModelePompeRepository extends CrudRepository<ModelePompe, Integer> {
	ModelePompe findByCode(Integer code);
	ModelePompe findByIsDeletedFalseAndLibelle(String libelle);
	List<ModelePompe> findByIsDeletedFalseOrderByLibelleAsc();
	List<ModelePompe> findByIsDeletedFalse();
	List<ModelePompe> findByIsDeletedTrue();
	List<ModelePompe> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<ModelePompe> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
