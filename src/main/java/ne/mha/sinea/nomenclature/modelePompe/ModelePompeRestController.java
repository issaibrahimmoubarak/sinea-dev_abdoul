package ne.mha.sinea.nomenclature.modelePompe;

import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class ModelePompeRestController {

	@Autowired
	ModelePompeRepository modelePompeService;
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/modelePompe")
	public int addModelePompe(@Validated ModelePompeForm modelePompeForm,BindingResult bindingResult, Model model) {
		ModelePompe savedModelePompe = new ModelePompe();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					ModelePompe P = new ModelePompe();
					P.setLibelle(modelePompeForm.getLibelle());
					savedModelePompe = modelePompeService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedModelePompe.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/updateModelePompe")
    public int updateModelePompe(@Validated ModelePompeForm modelePompeForm,BindingResult bindingResult, Model model) {
		ModelePompe savedModelePompe = new ModelePompe();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					ModelePompe P = modelePompeService.findByCode(modelePompeForm.getCode());
					P.setLibelle(modelePompeForm.getLibelle());
					savedModelePompe = modelePompeService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedModelePompe.getCode();
		
        
    }
	
	//@PreAuthorize("hasAuthority('gestion des types ressource')")
	@PostMapping("/deleteModelePompe")
    public int deleteModelePompe(@Validated ModelePompeForm modelePompeForm,BindingResult bindingResult, Model model) {
		ModelePompe savedModelePompe = new ModelePompe();
		if (!bindingResult.hasErrors()) {
			
			try {
				
					ModelePompe P = modelePompeService.findByCode(modelePompeForm.getCode());
					P.setIsDeleted(true);
					savedModelePompe = modelePompeService.save(P);
					
				}
			catch(Exception e){
				
				
				}
			
    	}
		else{
    		try{
    			
				}
    		catch(Exception e){
				
				}
    	}
		
		return savedModelePompe.getCode();
		
        
    }
}
