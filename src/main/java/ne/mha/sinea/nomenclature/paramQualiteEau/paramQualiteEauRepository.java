package ne.mha.sinea.nomenclature.paramQualiteEau;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface paramQualiteEauRepository extends CrudRepository<paramQualiteEau, Integer> {
	paramQualiteEau findByCode(Integer code);
	paramQualiteEau findByIsDeletedFalseAndLibelle(String libelle);
	List<paramQualiteEau> findByIsDeletedFalseOrderByLibelleAsc();
	List<paramQualiteEau> findByIsDeletedFalse();
	List<paramQualiteEau> findByIsDeletedTrue();
	List<paramQualiteEau> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<paramQualiteEau> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
