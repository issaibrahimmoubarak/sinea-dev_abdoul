package ne.mha.sinea.nomenclature.paramQualiteEau;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class paramQualiteEauForm {

	private int code;
	private String libelle;
	
}
