package ne.mha.sinea.nomenclature.typePrestationForage;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface typePrestationForageRepository extends CrudRepository<typePrestationForage, Integer> {
	typePrestationForage findByCode(Integer code);
	typePrestationForage findByIsDeletedFalseAndLibelle(String libelle);
	List<typePrestationForage> findByIsDeletedFalseOrderByLibelleAsc();
	List<typePrestationForage> findByIsDeletedFalse();
	List<typePrestationForage> findByIsDeletedTrue();
	List<typePrestationForage> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<typePrestationForage> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
