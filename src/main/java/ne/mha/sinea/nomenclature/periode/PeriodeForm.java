package ne.mha.sinea.nomenclature.periode;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PeriodeForm {

	private int code;
	private String libelle;
	private int parent_code;
	
}
