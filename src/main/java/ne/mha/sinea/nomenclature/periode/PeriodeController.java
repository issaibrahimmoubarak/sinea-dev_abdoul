package ne.mha.sinea.nomenclature.periode;


import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;

@Controller
public class PeriodeController {
	@Autowired
	PeriodeRepository periodeService;
	//@PreAuthorize("hasAuthority('gestion des périodes')")
	@GetMapping("/periode")
	public String  addPeriode(PeriodeForm periodeForm, Model model) {
		try{
			List<Periode> periodes = periodeService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("periodes", periodes);
			model.addAttribute("viewPath", "nomenclature/periode/periode");
			
			
			}
		catch(Exception e){
				
			}
		
		return Template.nomenclatureTemplate;

	}
	
	
	
}
