package ne.mha.sinea.nomenclature.typeRessourceStationHydrometrique;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TypeRessourceStationHydrometriqueForm {

	private int code;
	private String libelle;
	
}
