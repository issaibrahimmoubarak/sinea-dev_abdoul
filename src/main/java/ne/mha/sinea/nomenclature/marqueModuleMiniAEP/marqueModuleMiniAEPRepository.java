package ne.mha.sinea.nomenclature.marqueModuleMiniAEP;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface marqueModuleMiniAEPRepository extends CrudRepository<marqueModuleMiniAEP, Integer> {
	marqueModuleMiniAEP findByCode(Integer code);
	marqueModuleMiniAEP findByIsDeletedFalseAndLibelle(String libelle);
	List<marqueModuleMiniAEP> findByIsDeletedFalseOrderByLibelleAsc();
	List<marqueModuleMiniAEP> findByIsDeletedFalse();
	List<marqueModuleMiniAEP> findByIsDeletedTrue();
	List<marqueModuleMiniAEP> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<marqueModuleMiniAEP> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
