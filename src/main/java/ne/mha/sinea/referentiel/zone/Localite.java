package ne.mha.sinea.referentiel.zone;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ne.mha.sinea.CommonProperties;
import ne.mha.sinea.nomenclature.milieu.Milieu;

//annotation de lombok pour générer les getter et setter
@Data
//annotation de lombok pour générer le constructeur sans paramètre
@NoArgsConstructor
//annotation de lombok pour générer le constructeur avec tous les paramètres
@AllArgsConstructor
//indique qu'il s'agit d'une entité=> création d'une table au niveau de la base de données
@Entity
@Table(name = "localite")
public class Localite extends CommonProperties{
	//annotation pour indiquier l'identifiant d'une table
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "code")
	private Integer code;
	@NotNull
	@Column(name = "libelle")
	private String libelle;
	@Column(name = "code_ins")
	private String codeINS;
	@JoinColumn(name = "code_type_localite", referencedColumnName = "code")
	@ManyToOne
	private TypeLocalite typeLocalite;
	@JoinColumn(name = "code_localite_rattachement", referencedColumnName = "code")
	@ManyToOne
	private Localite rattachement;
	
	@JoinColumn(name = "code_commune", referencedColumnName = "code")
	@ManyToOne
	private Zone commune;
	@Column(name = "latitude",nullable = true)
	private Double latitude;
	@Column(name = "longitude",nullable = true)
	private Double longitude;
	
	@JoinColumn(name = "code_milieu", referencedColumnName = "code")
	@ManyToOne
	private Milieu milieu;
	
}
