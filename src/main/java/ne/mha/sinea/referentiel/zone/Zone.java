package ne.mha.sinea.referentiel.zone;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.sun.istack.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ne.mha.sinea.CommonProperties;
import ne.mha.sinea.nomenclature.milieu.Milieu;

//annotation de lombok pour générer les getter et setter
@Data
//annotation de lombok pour générer le constructeur sans paramètre
@NoArgsConstructor
//annotation de lombok pour générer le constructeur avec tous les paramètres
@AllArgsConstructor
//indique qu'il s'agit d'une entité=> création d'une table au niveau de la base de données
@Entity
@Table(name = "zone")
public class Zone extends CommonProperties {
	// annotation pour indiquier l'identifiant d'une table
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "code")
	private Integer code;
	@NotNull
	
	//@Column(name = "libelle",unique=true,columnDefinition="TEXT")
	@Column(name = "libelle",columnDefinition="TEXT")
	private String libelle;
	//relation reflexive côté fils => zone parent
	@JoinColumn(name = "parent_code", referencedColumnName = "code")
	@ManyToOne
    private Zone parent;
	//annotation pour une relation de type 1-N
    @OneToMany(mappedBy = "parent", cascade = CascadeType.ALL)
    private List<Zone> zone_rattaches = new ArrayList<>();
    @Column(name = "niveau")
    private int niveau;
    
    @JoinColumn(name = "code_milieu", referencedColumnName = "code")
	@ManyToOne
	private Milieu milieu;

}
