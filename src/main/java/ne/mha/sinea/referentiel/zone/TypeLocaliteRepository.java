package ne.mha.sinea.referentiel.zone;

import java.util.List;
import org.springframework.data.repository.CrudRepository;

public interface TypeLocaliteRepository extends CrudRepository<TypeLocalite, Integer> {
	TypeLocalite findByCode(Integer code);
	TypeLocalite findByLibelle(String libelle);
	TypeLocalite findByIsDeletedFalseAndLibelle(String libelle);
	List<TypeLocalite> findByIsDeletedFalseOrderByLibelleAsc();
	List<TypeLocalite> findByIsDeletedFalse();
	List<TypeLocalite> findByIsDeletedTrue();

}
