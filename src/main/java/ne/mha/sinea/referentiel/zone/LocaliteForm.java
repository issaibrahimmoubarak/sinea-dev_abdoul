package ne.mha.sinea.referentiel.zone;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LocaliteForm {
	private String libelle;
	private String codeINS;
	private Integer codeTypeLocalite;
	private Integer codeLocaliteRattachement;
	private Integer codeCommune;
	private double latitude;
	private double longitude;
}
