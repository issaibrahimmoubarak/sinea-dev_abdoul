package ne.mha.sinea.referentiel.indicateur.indicateur;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class IndicateurForm {

	private int code;
	private String libelle;
	
}
