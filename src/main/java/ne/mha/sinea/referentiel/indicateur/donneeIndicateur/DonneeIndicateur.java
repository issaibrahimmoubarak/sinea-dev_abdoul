package ne.mha.sinea.referentiel.indicateur.donneeIndicateur;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ne.mha.sinea.CommonProperties;
import ne.mha.sinea.nomenclature.periode.Periode;
import ne.mha.sinea.nomenclature.unite.Unite;
import ne.mha.sinea.referentiel.indicateur.indicateur.Indicateur;
import ne.mha.sinea.referentiel.indicateur.source.Source;
import ne.mha.sinea.referentiel.indicateur.sousGroupe.SousGroupe;
import ne.mha.sinea.referentiel.zone.Zone;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "donnee_indicateur")
public class DonneeIndicateur extends CommonProperties{

	@EmbeddedId
	private DonneeIndicateurKey code;
	@MapsId("codeIndicateur")
	@JoinColumn(name = "code_indicateur", referencedColumnName = "code")
	@ManyToOne
	private Indicateur indicateur;
	@MapsId("codeSource")
	@JoinColumn(name = "code_source", referencedColumnName = "code")
	@ManyToOne
	private Source source;
	@MapsId("codePeriode")
	@JoinColumn(name = "code_periode", referencedColumnName = "code")
	@ManyToOne
	private Periode periode;
	@MapsId("codeZone")
	@JoinColumn(name = "code_zone", referencedColumnName = "code")
	@ManyToOne
	private Zone zone;
	@MapsId("codeSousGroupe")
	@JoinColumn(name = "code_sous_groupe", referencedColumnName = "code")
	@ManyToOne
	private SousGroupe sousGroupe;
	@MapsId("codeUnite")
	@JoinColumn(name = "code_unite", referencedColumnName = "code")
	@ManyToOne
	private Unite unite;
	private double valeur;
}
