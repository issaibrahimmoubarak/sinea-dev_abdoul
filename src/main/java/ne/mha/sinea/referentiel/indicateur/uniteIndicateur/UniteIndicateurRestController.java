package ne.mha.sinea.referentiel.indicateur.uniteIndicateur;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import ne.mha.sinea.nomenclature.unite.Unite;
import ne.mha.sinea.nomenclature.unite.UniteRepository;
import ne.mha.sinea.referentiel.indicateur.indicateur.Indicateur;
import ne.mha.sinea.referentiel.indicateur.indicateur.IndicateurRepository;

@RestController
@ResponseBody
public class UniteIndicateurRestController {

	@Autowired
	UniteIndicateurRepository uniteIndicateurService;
	
	@Autowired
	IndicateurRepository indicateurService;
	
	@Autowired
	UniteRepository uniteService;
	
	//@PreAuthorize("hasAuthority('gestion des indicateurs')")
	@GetMapping("/listeUnitesIndicateur/{code}")
	public String  listeUnitesIndicateur(@PathVariable("code") int code) {
		String res = "";
		try{
			List<UniteIndicateur> uniteIndicateur = uniteIndicateurService.findByIsDeletedFalseAndIndicateur_Code(code);
			for(int i=0;i<uniteIndicateur.size();i++)
			{
				if(i==0)
					res += "{ id:"+uniteIndicateur.get(i).getUnite().getCode()+", name:\""+uniteIndicateur.get(i).getUnite().getLibelle()+"\"}";
				else
					res += ",{ id:"+uniteIndicateur.get(i).getUnite().getCode()+", name:\""+uniteIndicateur.get(i).getUnite().getLibelle()+"\"}";
				
			}
			res = "["+res+"]";

			}
		catch(Exception e){
			System.out.println(e);
			}
		return res;
	}
	
	//@PreAuthorize("hasAuthority('gestion des indicateurs')")
	@GetMapping("/getUniteIndicateurDisponibles/{code}")
	public String  getUniteIndicateurDisponibles(@PathVariable("code") int code) {
		String res = "";
		try{
			List<Unite> unitesDisponibles = new ArrayList<Unite>();
			List<Integer> unitesUtilises = new ArrayList<Integer>();
			List<UniteIndicateur> uniteIndicateur = uniteIndicateurService.findByIsDeletedFalseAndIndicateur_Code(code);
			for(int i=0;i<uniteIndicateur.size();i++)
			{
				unitesUtilises.add(uniteIndicateur.get(i).getUnite().getCode());
			}
			if(unitesUtilises.isEmpty())
				unitesDisponibles = uniteService.findByIsDeletedFalse();
			else
				unitesDisponibles = uniteService.findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(unitesUtilises);
			for(int i=0;i<unitesDisponibles.size();i++)
			{
				if(i==0)
					res += "{ id:"+unitesDisponibles.get(i).getCode()+", pId:0, name:\""+unitesDisponibles.get(i).getLibelle()+"\"}";
				else
					res += ",{ id:"+unitesDisponibles.get(i).getCode()+", pId:0, name:\""+unitesDisponibles.get(i).getLibelle()+"\"}";
				
			}
			if(unitesDisponibles.size() >0)
				res = "[{ id:0, pId:0, name:\"/\", open:true},"+res+"]";
			else
				res = "[{ id:0, pId:0, name:\"/\", open:true}]";
			
			}
		catch(Exception e){
			System.out.println(e);
			}
		return res;
	}
	
	//@PreAuthorize("hasAuthority('gestion des indicateurs')")
	@PostMapping("/ajoutUniteIndicateur")
	public String ajoutUniteIndicateurSubmit(@RequestParam(value="params[]") Integer[] params,Integer indicateurId) {
		try{
				Indicateur indicateur = indicateurService.findByCode(indicateurId); 
				for(int i=0;i<params.length;i++)
				{
					Unite unite = uniteService.findByCode(params[i]);
					UniteIndicateur io = new UniteIndicateur();
					io.setCode(new UniteIndicateurKey(indicateur.getCode(),unite.getCode()));
					io.setIndicateur(indicateur);
					io.setUnite(unite);
					uniteIndicateurService.save(io);
					
				}
			
			}
		catch(Exception e){
			System.out.println(e);
			}
			
		return "ok";
	}
	
	//@PreAuthorize("hasAuthority('gestion des indicateurs')")
	@PostMapping("/suppressionUniteIndicateur")
	public String suppressionUniteIndicateurSubmit(@RequestParam(value="params[]") Integer[] params,Integer indicateurId) {
		try{
				for(int i=0;i<params.length;i++)
				{
					UniteIndicateur io = uniteIndicateurService.findByIsDeletedFalseAndUnite_CodeAndIndicateur_Code(params[i],indicateurId);
					uniteIndicateurService.delete(io);
					
				}
			
			}
		catch(Exception e){
			System.out.println(e);
			}
			
		return "ok";
	}
	
}
