package ne.mha.sinea.referentiel.indicateur.source;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface SourceRepository extends CrudRepository<Source, Integer> {
	Source findByCode(Integer code);
	Source findByIsDeletedFalseAndLibelle(String libelle);
	List<Source> findByIsDeletedFalseOrderByLibelleAsc();
	List<Source> findByIsDeletedFalse();
	List<Source> findByIsDeletedTrue();
	List<Source> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<Source> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
