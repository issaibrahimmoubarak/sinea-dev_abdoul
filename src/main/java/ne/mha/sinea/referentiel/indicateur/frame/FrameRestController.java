package ne.mha.sinea.referentiel.indicateur.frame;

import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
//import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
@RestController
@ResponseBody
public class FrameRestController {

	@Autowired
	FrameRepository frameService;
	
	@Autowired
	SectionFrameRepository sectionFrameService;
	//@PreAuthorize("hasAuthority('gestion des pages de saisie')")
	@PostMapping("/ajoutFrame")
	public String ajoutDonneeIndicateurSubmit(@RequestParam(value="nom_frame") String nom_frame,@RequestParam(value="params[]") Integer[] params) {
		try{
			Frame frame = new Frame();
			ArrayList<SectionFrame> sectionFrames = new ArrayList<SectionFrame>();
			for(int i=0;i<params.length;i++)
			{
				SectionFrame sectionFrame = sectionFrameService.findByCode(params[i]);
				sectionFrames.add(sectionFrame);
				
			}
			frame.setLibelle(nom_frame);
			frame.setSectionFrames(sectionFrames);
			frameService.save(frame);
		
			}
		catch(Exception e){
			System.out.println(e);
			}
			
		return "ok";
	}
	
	//@PreAuthorize("hasAuthority('gestion des pages de saisie')")
	@PostMapping("/modifierFrame")
	public String modifierFrameSubmit(@RequestParam(value="id_frame") Integer id_frame,@RequestParam(value="nom_frame") String nom_frame,@RequestParam(value="params[]") Integer[] params) {
		try{
			//Frame frame = new Frame();
			Frame frame = frameService.findByCode(id_frame);
			ArrayList<SectionFrame> sectionFrames = new ArrayList<SectionFrame>();
			for(int i=0;i<params.length;i++)
			{
				SectionFrame sectionFrame = sectionFrameService.findByCode(params[i]);
				sectionFrames.add(sectionFrame);
				
			}
			//frame.setCode(frame2.getCode());
			frame.setLibelle(nom_frame);
			if(!sectionFrames.isEmpty())
				frame.setSectionFrames(sectionFrames);
			else
				frame.setSectionFrames(null);
			//frameService.delete(frame2);
			frameService.save(frame);
		
			}
		catch(Exception e){
			System.out.println(e);
			}
			
		return "ok";
	}
	
	

}
