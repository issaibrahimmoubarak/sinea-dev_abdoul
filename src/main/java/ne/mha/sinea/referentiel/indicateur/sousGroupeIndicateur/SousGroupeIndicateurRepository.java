package ne.mha.sinea.referentiel.indicateur.sousGroupeIndicateur;

import java.util.List;
import org.springframework.data.repository.CrudRepository;

public interface SousGroupeIndicateurRepository extends CrudRepository<SousGroupeIndicateur, SousGroupeIndicateurKey>  {

	List<SousGroupeIndicateur> findByIsDeletedFalse();
	List<SousGroupeIndicateur> findByIsDeletedTrue();
	List<SousGroupeIndicateur> findByIsDeletedFalseAndIndicateur_Code(Integer code);
	SousGroupeIndicateur findByIsDeletedFalseAndSousGroupe_CodeAndIndicateur_Code(Integer codeIndicateur, Integer codeSousGroupe);
	
}
