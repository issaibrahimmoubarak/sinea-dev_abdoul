package ne.mha.sinea.referentiel.indicateur.classification;

/*
 * opérations sur la table (base de données)
 */
import java.util.List;
import org.springframework.data.repository.CrudRepository;

public interface ClassificationRepository extends CrudRepository<Classification, Integer> {
	Classification findByCode(Integer code);
	Classification findByIsDeletedFalseAndLibelle(String libelle);
	Classification findByIsDeletedFalseAndLibelleAndParent_Libelle(String libelle,String libelleParent);
	Classification findByIsDeletedFalseAndParent_Libelle(String libelle);
	Classification findByIsDeletedFalseAndParent_Code(Integer code);
	List<Classification> findByIsDeletedFalseOrderByLibelleAsc();
	List<Classification> findByIsDeletedFalseAndTypeClassification_CodeOrderByLibelleAsc(int code);
	List<Classification> findByIsDeletedFalseAndNiveau(int niveau);
	List<Classification> findByIsDeletedFalse();
	List<Classification> findByIsDeletedTrue();
	
}
