package ne.mha.sinea.referentiel.indicateur.frame;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import ne.mha.sinea.Template;
import ne.mha.sinea.nomenclature.periode.PeriodeRepository;
import ne.mha.sinea.nomenclature.typeClassification.TypeClassification;
import ne.mha.sinea.nomenclature.typeClassification.TypeClassificationRepository;
import ne.mha.sinea.referentiel.indicateur.classification.ClassificationRepository;
import ne.mha.sinea.referentiel.indicateur.indicateur.IndicateurRepository;
import ne.mha.sinea.referentiel.indicateur.source.SourceRepository;
import ne.mha.sinea.referentiel.zone.ZoneRepository;

@Controller
public class FrameController {

	@Autowired
	IndicateurRepository indicateurService;
	@Autowired
	ClassificationRepository classificationService;
	@Autowired
	PeriodeRepository periodeService;
	@Autowired
	ZoneRepository zoneService;
	@Autowired
	SourceRepository sourceService;
	
	@Autowired
	TypeClassificationRepository typeClassificationService;
	
	@Autowired
	SectionFrameRepository sectionFrameService;
	
	@Autowired
	FrameRepository frameService;

	//@PreAuthorize("hasAuthority('gestion des pages de saisie')")
	@GetMapping("/frame")
	public String  frame(Model model) {
		try{
			
			List<TypeClassification> typeClassifications = typeClassificationService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("typeClassifications", typeClassifications);
			
			List<SectionFrame> sectionFrames = sectionFrameService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("sectionFrames", sectionFrames);
				
			model.addAttribute("horizontalMenu", "horizontalMenu");
			model.addAttribute("sidebarMenu", "configurationSidebarMenu");
			model.addAttribute("breadcrumb", "breadcrumb");
			model.addAttribute("navigationPath", "Referentiel/Nouveau Frame");
			model.addAttribute("viewPath", "referentiel/frame/nouveauframe");
		
		
		}
		catch(Exception e){
				
			}
		return Template.defaultTemplate;
	}
	
	//@PreAuthorize("hasAuthority('gestion des pages de saisie')")
	@GetMapping("/editframe/{code}")
	public String  editFrame(@PathVariable("code") int code,Model model) {
		try{
			
			Frame frame = frameService.findByCode(code);
			model.addAttribute("frame", frame);
			List<TypeClassification> typeClassifications = typeClassificationService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("typeClassifications", typeClassifications);
			
			List<SectionFrame> sectionFrames = sectionFrameService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("sectionFrames", sectionFrames);
				
			model.addAttribute("horizontalMenu", "horizontalMenu");
			model.addAttribute("sidebarMenu", "configurationSidebarMenu");
			model.addAttribute("breadcrumb", "breadcrumb");
			model.addAttribute("navigationPath", "Referentiel/Frame");
			model.addAttribute("viewPath", "referentiel/frame/modifierFrame");
		
		
		}
		catch(Exception e){
				
			}
		return Template.defaultTemplate;
	}
	//@PreAuthorize("hasAuthority('gestion des pages de saisie')")
	@GetMapping("/frame/{code}")
	public String  saisieFrame(@PathVariable("code") int code,Model model) {
		try{
			
			Frame frame = frameService.findByCode(code);
			model.addAttribute("frame", frame);
			List<TypeClassification> typeClassifications = typeClassificationService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("typeClassifications", typeClassifications);
			
			List<SectionFrame> sectionFrames = sectionFrameService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("sectionFrames", sectionFrames);
				
			model.addAttribute("horizontalMenu", "horizontalMenu");
			model.addAttribute("sidebarMenu", "configurationSidebarMenu");
			model.addAttribute("breadcrumb", "breadcrumb");
			model.addAttribute("navigationPath", "Referentiel/Frame");
			model.addAttribute("viewPath", "referentiel/frame/frame");
		
		
		}
		catch(Exception e){
				
			}
		return Template.defaultTemplate;
	}
	
	
}
