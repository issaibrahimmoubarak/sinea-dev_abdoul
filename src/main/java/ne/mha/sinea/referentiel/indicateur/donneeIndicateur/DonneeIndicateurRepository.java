package ne.mha.sinea.referentiel.indicateur.donneeIndicateur;

import java.util.List;
import org.springframework.data.repository.CrudRepository;

public interface DonneeIndicateurRepository extends CrudRepository<DonneeIndicateur, DonneeIndicateurKey>  {

	List<DonneeIndicateur> findByIsDeletedFalse();
	List<DonneeIndicateur> findByIsDeletedTrue();
	List<DonneeIndicateur> findByIsDeletedFalseAndIndicateur_Code(Integer code);
	//DonneeIndicateur findByIsDeletedFalseAndIndicateur_CodeAndSource_CodeAndPeriode_CodeAndZone_CodeAndSousGroupe_Code(Integer codeIndicateur, Integer codeSource, Integer codePeriode, Integer codeZone, Integer codeSousGroupe);
	DonneeIndicateur findByIsDeletedFalseAndSource_CodeAndIndicateur_CodeAndZone_CodeAndSousGroupe_CodeAndUnite_CodeAndPeriode_Code(Integer codeSource,Integer codeIndicateur,Integer codeZone, Integer codeSousGroupe, Integer codeUnite, Integer codePeriode);
	
}
