package ne.mha.sinea.referentiel.indicateur.donneeIndicateur;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
//import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import ne.mha.sinea.nomenclature.periode.Periode;
import ne.mha.sinea.nomenclature.periode.PeriodeRepository;
import ne.mha.sinea.nomenclature.unite.Unite;
import ne.mha.sinea.nomenclature.unite.UniteRepository;
import ne.mha.sinea.referentiel.indicateur.indicateur.Indicateur;
import ne.mha.sinea.referentiel.indicateur.indicateur.IndicateurRepository;
import ne.mha.sinea.referentiel.indicateur.source.Source;
import ne.mha.sinea.referentiel.indicateur.source.SourceRepository;
import ne.mha.sinea.referentiel.indicateur.sousGroupe.SousGroupe;
import ne.mha.sinea.referentiel.indicateur.sousGroupe.SousGroupeRepository;
import ne.mha.sinea.referentiel.zone.Zone;
import ne.mha.sinea.referentiel.zone.ZoneRepository;


@RestController
@ResponseBody
public class DonneeIndicateurRestController {

	@Autowired
	IndicateurRepository indicateurService;
	@Autowired
	SourceRepository sourceService;
	@Autowired
	PeriodeRepository periodeService;
	@Autowired
	ZoneRepository zoneService;
	@Autowired
	SousGroupeRepository sousGroupeService;
	@Autowired
	UniteRepository uniteService;
	@Autowired
	DonneeIndicateurRepository donneeIndicateurService;
	
	//@PreAuthorize("hasAuthority('mise à jour des données')")
	@PostMapping("/ajoutDonneeIndicateur")
	public String ajoutDonneeIndicateurSubmit(@RequestParam(value="params[]") String[] datas) {
		try{
				for(int i=0;i<datas.length;i++)
				{
					String str = datas[i];
					String[] data = str.split("/");
					Indicateur indicateur = indicateurService.findByCode(Integer.valueOf(data[0]));
					Source source = sourceService.findByCode(Integer.valueOf(data[1]));
					Periode periode = periodeService.findByCode(Integer.valueOf(data[2]));
					Zone zone = zoneService.findByCode(Integer.valueOf(data[3]));
					SousGroupe sousGroupe = sousGroupeService.findByCode(Integer.valueOf(data[4]));
					Unite unite = uniteService.findByCode(Integer.valueOf(data[5]));
					double valeur =0;
					if(data.length>6)
						valeur = Double.parseDouble(data[6]);
					
					DonneeIndicateur di = new DonneeIndicateur();
					di.setCode(new DonneeIndicateurKey(indicateur.getCode(),source.getCode(),periode.getCode(),zone.getCode(),sousGroupe.getCode(),unite.getCode()));
					di.setIndicateur(indicateur);
					di.setSource(source);
					di.setPeriode(periode);
					di.setZone(zone);
					di.setSousGroupe(sousGroupe);
					di.setUnite(unite);
					di.setValeur(valeur);
					if(data.length>6)
						donneeIndicateurService.save(di);
					
				}
			
			}
		catch(Exception e){
			System.out.println(e);
			}
			
		return "ok";
	}
	//@PreAuthorize("hasAuthority('mise à jour des données')")
	@PostMapping("/getDonneeIndicateur")
	public String getDonneeIndicateur(@RequestParam(value="params[]") double[] data) {
		String valeur = "";
		try{
					Indicateur indicateur = indicateurService.findByCode((int)data[0]);
					Source source = sourceService.findByCode((int)data[1]);
					Periode periode = periodeService.findByCode((int)data[2]);
					Zone zone = zoneService.findByCode((int)data[3]);
					SousGroupe sousGroupe = sousGroupeService.findByCode((int)data[4]);
					Unite unite = uniteService.findByCode((int)data[5]);
					
					DonneeIndicateur d = donneeIndicateurService.findByIsDeletedFalseAndSource_CodeAndIndicateur_CodeAndZone_CodeAndSousGroupe_CodeAndUnite_CodeAndPeriode_Code(source.getCode(), indicateur.getCode(), zone.getCode(), sousGroupe.getCode(), unite.getCode(), periode.getCode());
					if(d != null) valeur = d.getValeur()+"";
					
			
			}
		catch(Exception e){
			System.out.println(e);
			}
			
		return valeur;
	}
	//@PreAuthorize("hasAuthority('mise à jour des données')")
	@PostMapping("/exportDonneeIndicateur")
	public ResponseEntity<Resource> exportDonneeIndicateur(@RequestParam(value="data[]") String[] datas,@RequestParam(value="periodes[]") Integer[] periodes) {
		String[] header = {"indicateur","id_indicateur","zone","id_zone","sous_groupe","id_sous_groupe","unite","id_unite","source","id_source"};
		
		// nom du fichier excel qui servira d'export
		String filename = "exportDonneeIndicateur.xlsx";
		// effectuer l'export via l'objet donneesSocioEcoExploitantExcelService
		try (Workbook workbook = new XSSFWorkbook(); ByteArrayOutputStream out = new ByteArrayOutputStream();) {
		      Sheet sheet = workbook.createSheet("donneesIndicateur");
		      // création de l'entête du fichier excel
		      Row headerRow = sheet.createRow(0);
		      for (int col = 0; col < header.length; col++) {
		        Cell cell = headerRow.createCell(col);
		        cell.setCellValue(header[col]);
		      }
		      for (int col = header.length; col < (header.length +periodes.length); col++) {
			        Cell cell = headerRow.createCell(col);
			        Periode periode = periodeService.findByCode(periodes[col-header.length]);
			        cell.setCellValue(periode.getLibelle());
			      }

		      int rowIdx = 1;
		      for(int i=0;i<datas.length;i++)
				{
		    	  	Row row = sheet.createRow(rowIdx);
					String str = datas[i];
					String[] data = str.split("/");
					Indicateur indicateur = indicateurService.findByCode(Integer.valueOf(data[0]));
					Zone zone = zoneService.findByCode(Integer.valueOf(data[1]));
					SousGroupe sousGroupe = sousGroupeService.findByCode(Integer.valueOf(data[2]));
					Unite unite = uniteService.findByCode(Integer.valueOf(data[3]));
					Source source = sourceService.findByCode(Integer.valueOf(data[4]));
					
					row.createCell(0).setCellValue(indicateur.getLibelle());
					row.createCell(1).setCellValue(indicateur.getCode());
					row.createCell(2).setCellValue(zone.getLibelle());
					row.createCell(3).setCellValue(zone.getCode());
					row.createCell(4).setCellValue(sousGroupe.getLibelle());
					row.createCell(5).setCellValue(sousGroupe.getCode());
					row.createCell(6).setCellValue(unite.getLibelle());
					row.createCell(7).setCellValue(unite.getCode());
					row.createCell(8).setCellValue(source.getLibelle());
					row.createCell(9).setCellValue(source.getCode());
					
					for (int j = 5; j < data.length; j++) {
						String[] valeurAndPeriode = data[j].split("@");
				        if((valeurAndPeriode.length>1) && (periodes[j-5]==Integer.valueOf(valeurAndPeriode[1])))
				        {
				        	if(!valeurAndPeriode[0].isEmpty())
				        		row.createCell(10+(j-5)).setCellValue(Double.parseDouble(valeurAndPeriode[0]));
				        	else
				        		row.createCell(10+(j-5)).setCellValue("");
				        }
				        else if((valeurAndPeriode.length==1) && (periodes[j-5]==Integer.valueOf(valeurAndPeriode[0])))
				        {
				        	row.createCell(10+(j-5)).setCellValue("");
				        }
				        else
				        {
				        	row.createCell(10+(j-5)).setCellValue("");
				        }
				        	
				      }
					
					
					rowIdx++;
				}
		      
		      sheet = workbook.createSheet("annee");
		      headerRow = sheet.createRow(0);
		      Cell cell = headerRow.createCell(0);
		      cell.setCellValue("id");
		      cell = headerRow.createCell(1);
		      cell.setCellValue("libelle");
		      
		      rowIdx = 1;
		      for(int i=0;i<periodes.length;i++)
		    	{
		    	    Periode periode = periodeService.findByCode(periodes[i]);
		    		Row row = sheet.createRow(rowIdx++);
		    		row.createCell(0).setCellValue(periode.getCode());
		    		row.createCell(1).setCellValue(periode.getLibelle());
		      
		    	}
		      
		      workbook.write(out);
		      InputStreamResource file = new InputStreamResource(new ByteArrayInputStream(out.toByteArray()));
		     
		     return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + filename)
						.contentType(MediaType.parseMediaType("application/vnd.ms-excel")).body(file);
			
		      
		    } catch (IOException e) {
		      throw new RuntimeException(e.getMessage());
		    }
		
		
	}
}
