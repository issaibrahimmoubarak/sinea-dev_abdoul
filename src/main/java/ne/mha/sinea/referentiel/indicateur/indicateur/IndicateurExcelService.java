package ne.mha.sinea.referentiel.indicateur.indicateur;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class IndicateurExcelService {
	
	@Autowired
	IndicateurRepository indicateurService;
	static String[] header = {"libelle"};
	
	//placer le contenu du fichier excel dans un objet list
	public  List<Indicateur> excelToIndicateur(InputStream is) {
		List<Indicateur> listeIndicateurs = new ArrayList<Indicateur>();
		try {
	    
		  //instancier un classeur Excel
	      Workbook workbook = new XSSFWorkbook(is);
	      //accéder à la première feuille
	      Sheet sheet = workbook.getSheetAt(0);
	      //partcourir les lignes de la feuille
	      Iterator<Row> rows = sheet.iterator();
	      int rowNumber = 0;
	      while (rows.hasNext()) {
	        Row currentRow = rows.next();
	        // ignorer l'entête du fichier Excel lors de l'import
	        if (rowNumber == 0) {
	          rowNumber++;
	          continue;
	        }
	        //verifier que la ligne à importer  n'est pas vide
	        if(!isRowEmpty(currentRow)) {
		        Iterator<Cell> cellsInRow = currentRow.iterator();
		        Indicateur indicateur = new Indicateur();
		        int cellIdx = 0;
		        //parcourir les colonnes d'une ligne
		        while (cellsInRow.hasNext()) {
		          Cell currentCell = cellsInRow.next();
		          switch (cellIdx) {
		          //première colonne
		          case 0:
		        	  indicateur.setLibelle(currentCell.getStringCellValue());
		        	  break;
		          }
		          
		          cellIdx++;
		        }
		        //ajouter la ligne récuperer à la liste des région (il s'agit pas encore de la sauvegarde dans la base
		        listeIndicateurs.add(indicateur);
	        }
	      } 

	      workbook.close();
	      
	    } catch (IOException e) {
	    	System.out.println(e);
	    }
		return listeIndicateurs;
	  }
		
	//sauvegarde des informations du fichier excel dans la base de données
	public List<String> importData(MultipartFile file) {
		List<String> unsavedIndicateur = new ArrayList<String>();
		try {
	      //récupération des lignes Excel dans un objet List
	      List<Indicateur> listIndicateurs = excelToIndicateur(file.getInputStream());
	      //sauvegarde des informations récuperés du fichier excel dans la base de données
	      
	      //indicateurService.saveAll(listIndicateurs);
	      for(int i=0;i<listIndicateurs.size();i++)
	      {
	    	  try {
	    		  indicateurService.save(listIndicateurs.get(i));
		    	  
	    	  }catch(Exception e)
	    	  {
	    		  unsavedIndicateur.add(listIndicateurs.get(i).getLibelle()); 
	    	  }
	    	  
	      }
	      
	    } catch (IOException e) {
	    	System.out.println(e);
	     
	    }
		return unsavedIndicateur;
	  }
		
	//mettre le les informations dans un objet ByteArrayInputStream en vue de la création du fichier excel
	 public  ByteArrayInputStream indicateurToExcel(List<Indicateur> indicateurs) {
	    try (Workbook workbook = new XSSFWorkbook(); ByteArrayOutputStream out = new ByteArrayOutputStream();) {
		      Sheet sheet = workbook.createSheet("indicateurs");
		      // création de l'entête du fichier excel
		      Row headerRow = sheet.createRow(0);
		      //création des colonnes de l'entête
		      for (int col = 0; col < header.length; col++) {
		        Cell cell = headerRow.createCell(col);
		        cell.setCellValue(header[col]);
		      }
	
		      int rowIdx = 1;
		      //création des lignes du fichier
		      for (Indicateur indicateur : indicateurs) {
		        Row row = sheet.createRow(rowIdx++);
		        row.createCell(0).setCellValue(indicateur.getLibelle());
		         
		      }
	
		      workbook.write(out);
		      return new ByteArrayInputStream(out.toByteArray());
	      
		    } catch (IOException e) {
		      throw new RuntimeException(e.getMessage());
		    }
	  	}
		 
		 //récuperaton des données de la base pour les convertir en ByteArrayInputStream
		 public ByteArrayInputStream  exportData() {  
	    	List<Indicateur> indicateurs = (List<Indicateur>) indicateurService.findByIsDeletedFalse();
	    	ByteArrayInputStream in = indicateurToExcel(indicateurs);
	    	return in;
		    
		  }
		 
		 //vérifier si une ligne est vide
		 private  boolean isRowEmpty(Row row) {
			boolean isEmpty = true;
			DataFormatter dataFormatter = new DataFormatter();
			if (row != null) {
				for (Cell cell : row) {
					if (dataFormatter.formatCellValue(cell).trim().length() > 0) {
						isEmpty = false;
						break;
					}
				}
			}

			return isEmpty;
		}
}
