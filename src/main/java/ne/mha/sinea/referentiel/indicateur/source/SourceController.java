package ne.mha.sinea.referentiel.indicateur.source;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import ne.mha.sinea.Template;
import ne.mha.sinea.nomenclature.typeClassification.TypeClassification;
import ne.mha.sinea.nomenclature.typeClassification.TypeClassificationRepository;
import ne.mha.sinea.referentiel.indicateur.frame.SectionFrame;
import ne.mha.sinea.referentiel.indicateur.frame.SectionFrameRepository;

@Controller
public class SourceController {
	@Autowired
	SourceRepository sourceService;
	@Autowired
	TypeClassificationRepository typeClassificationService;
	@Autowired
	SectionFrameRepository sectionFrameService;
	
	//@PreAuthorize("hasAuthority('gestion des sources')")
	@GetMapping("/source")
	public String  addSource(SourceForm sourceForm, Model model) {
		try{
			List<Source> sources = sourceService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("sources", sources);
			List<TypeClassification> typeClassifications = typeClassificationService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("typeClassifications", typeClassifications);
			List<SectionFrame> sectionFrames = sectionFrameService.findByIsDeletedFalseOrderByLibelleAsc();
			model.addAttribute("sectionFrames", sectionFrames);
			
			model.addAttribute("horizontalMenu", "horizontalMenu");
			model.addAttribute("sidebarMenu", "configurationSidebarMenu");
			model.addAttribute("breadcrumb", "breadcrumb");
			model.addAttribute("navigationPath", "Referentiel/Source");
			model.addAttribute("viewPath", "referentiel/source/source");
			
		}
		catch(Exception e){
				
		}
		
		return Template.defaultTemplate;

	}
	
		
}
