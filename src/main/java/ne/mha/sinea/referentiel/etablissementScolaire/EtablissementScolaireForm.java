package ne.mha.sinea.referentiel.etablissementScolaire;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EtablissementScolaireForm {

	private String denomination;
	private double latitude;
	private double longitude;
	private int anneeCreation;
	private Integer codeTypeEtablissementScolaire;
	private Integer codeLocalite;
}
