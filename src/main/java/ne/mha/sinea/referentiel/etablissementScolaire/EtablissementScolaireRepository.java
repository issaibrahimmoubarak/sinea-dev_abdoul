package ne.mha.sinea.referentiel.etablissementScolaire;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface EtablissementScolaireRepository extends CrudRepository<EtablissementScolaire, Integer> {
	EtablissementScolaire findByCode(Integer code);
	EtablissementScolaire findBydenomination(String denomination);
	List<EtablissementScolaire> findByIsDeletedFalse();
	List<EtablissementScolaire> findByIsDeletedTrue();
}
