package ne.mha.sinea.referentiel.reseauSuivi;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ReseauSuiviForm {

	private String nom;
	private String proprietaire;
	private String structureEnCharge;
	private String commentaire;
}
