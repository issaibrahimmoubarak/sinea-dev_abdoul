package ne.mha.sinea.referentiel.projet;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProjetForm {

	private int code;
	private String libelle;
	
}
