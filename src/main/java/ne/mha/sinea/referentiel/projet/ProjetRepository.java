package ne.mha.sinea.referentiel.projet;

import java.util.List;
import org.springframework.data.repository.CrudRepository;



public interface ProjetRepository extends CrudRepository<Projet, Integer> {
	Projet findByCode(Integer code);
	Projet findByIsDeletedFalseAndLibelle(String libelle);
	List<Projet> findByIsDeletedFalseOrderByLibelleAsc();
	List<Projet> findByIsDeletedFalse();
	List<Projet> findByIsDeletedTrue();
	List<Projet> findByIsDeletedFalseAndCodeNotInOrderByLibelleAsc(List<Integer> ListId);
	List<Projet> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);
	
	

}
