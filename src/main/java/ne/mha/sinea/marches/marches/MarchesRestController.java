package ne.mha.sinea.marches.marches;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import ne.mha.sinea.nomenclature.financement.Financement;
import ne.mha.sinea.nomenclature.financement.FinancementRepository;
import ne.mha.sinea.nomenclature.intervenant.Intervenant;
import ne.mha.sinea.nomenclature.intervenant.IntervenantRepository;
import ne.mha.sinea.nomenclature.modePassation.ModePassation;
import ne.mha.sinea.nomenclature.modePassation.ModePassationRepository;
import ne.mha.sinea.nomenclature.typeMarches.TypeMarches;
import ne.mha.sinea.nomenclature.typeMarches.TypeMarchesRepository;

@org.springframework.web.bind.annotation.RestController
@ResponseBody
public class MarchesRestController {
    @Autowired
	FinancementRepository financementService;
    @Autowired
	ModePassationRepository modePassationService;
    @Autowired
	TypeMarchesRepository typeMarchesService;
    @Autowired
	IntervenantRepository intervenantService;
    @Autowired
	MarchesRepository marchesService;

    @PostMapping("/addMarches")
	public int addMarches(@Validated MarchesForm marchesForm, BindingResult bindingResult, Model model) {
		Marches savedMarches = new Marches();
		if (!bindingResult.hasErrors()) {

			try {
				
					Marches M = new Marches();
					M.setAnneeMarchesPublique(marchesForm.getAnneeMarchesPublique());
                    M.setObjet(marchesForm.getObjet());
                    M.setCoutPrevisionnel(marchesForm.getCoutPrevisionnel());
                    M.setDateLancementProcedure(marchesForm.getDateLancementProcedure());
                    M.setDateAttributionContrat(marchesForm.getDateAttributionContrat());
                    M.setDateDemarragePrestation(marchesForm.getDateDemarragePrestation());
                    M.setDateAchevementPrestation(marchesForm.getDateAchevementPrestation());
                    M.setIntervenant(intervenantService.findByCode(marchesForm.getCodeIntervenant()));
                    M.setModePassation(modePassationService.findByCode(marchesForm.getCodeModePassation()));
                    M.setFinancement(financementService.findByCode(marchesForm.getCodeSourceFinancement()));
                    M.setTypeMarches(typeMarchesService.findByCode(marchesForm.getCodeTypeMarches()));
				
					savedMarches = marchesService.save(M);

                    
				//récuperation de la liste des Marches de la base de données et envoie de la liste récuperée à la vue à travers le modele
				List<Marches> marches =  (List<Marches>) marchesService.findByIsDeletedFalse();
				model.addAttribute("marches", marches);
				List<Intervenant> intervenants =  (List<Intervenant>) intervenantService.findByIsDeletedFalse();
				model.addAttribute("intervenants", intervenants);
				List<ModePassation> modePassations =  (List<ModePassation>) modePassationService.findByIsDeletedFalse();
				model.addAttribute("modePassations", modePassations);
				List<Financement> financements =  (List<Financement>) financementService.findByIsDeletedFalse();
				model.addAttribute("sourceFinancements", financements);
				List<TypeMarches> typeMarches =  (List<TypeMarches>) typeMarchesService.findByIsDeletedFalse();
				model.addAttribute("typeMarches", typeMarches);

				//envoie de la liste récuperée à la vue à travers le modele
				model.addAttribute("marches", marches);
				//indiquer que l'operation d'ajout a réussie
				model.addAttribute("operationStatus", "operationStatus/success");
				model.addAttribute("horizontalMenu", "horizontalMenu");
				model.addAttribute("sidebarMenu", "configurationSidebarMenu");
				model.addAttribute("breadcrumb", "breadcrumb");
				model.addAttribute("navigationPath", "marches/marches");
				model.addAttribute("viewPath", "marcher/marches/marches");
				
					
				}
			catch(Exception e){
                //récuperation de la liste des Marches de la base de données et envoie de la liste récuperée à la vue à travers le modele
				List<Marches> marches =  (List<Marches>) marchesService.findByIsDeletedFalse();
				model.addAttribute("marches", marches);
				List<Intervenant> intervenants =  (List<Intervenant>) intervenantService.findByIsDeletedFalse();
				model.addAttribute("intervenants", intervenants);
				List<ModePassation> modePassations =  (List<ModePassation>) modePassationService.findByIsDeletedFalse();
				model.addAttribute("modePassations", modePassations);
				List<Financement> financements =  (List<Financement>) financementService.findByIsDeletedFalse();
				model.addAttribute("sourceFinancements", financements);
				List<TypeMarches> typeMarches =  (List<TypeMarches>) typeMarchesService.findByIsDeletedFalse();
				model.addAttribute("typeMarches", typeMarches);
			

                model.addAttribute("operationStatus", "operationStatus/unsuccess");
				model.addAttribute("horizontalMenu", "horizontalMenu");
				model.addAttribute("sidebarMenu", "configurationSidebarMenu");
				model.addAttribute("breadcrumb", "breadcrumb");
				model.addAttribute("navigationPath", "marches/marches");
				model.addAttribute("viewPath", "marches/marches/marches");
				}

                // return "YES";
    	}
		else{
    		try{
                //récuperation de la liste des Marches de la base de données et envoie de la liste récuperée à la vue à travers le modele
				List<Marches> marches =  (List<Marches>) marchesService.findByIsDeletedFalse();
				model.addAttribute("marches", marches);
				List<Intervenant> intervenants =  (List<Intervenant>) intervenantService.findByIsDeletedFalse();
				model.addAttribute("intervenants", intervenants);
				List<ModePassation> modePassations =  (List<ModePassation>) modePassationService.findByIsDeletedFalse();
				model.addAttribute("modePassations", modePassations);
				List<Financement> financements =  (List<Financement>) financementService.findByIsDeletedFalse();
				model.addAttribute("sourceFinancements", financements);
				List<TypeMarches> typeMarches =  (List<TypeMarches>) typeMarchesService.findByIsDeletedFalse();
				model.addAttribute("typeMarches", typeMarches);

    			model.addAttribute("operationStatus", "operationStatus/unsuccess");	
				model.addAttribute("horizontalMenu", "horizontalMenu");
				model.addAttribute("sidebarMenu", "configurationSidebarMenu");
				model.addAttribute("breadcrumb", "breadcrumb");
				model.addAttribute("navigationPath", "marches/marches");
				model.addAttribute("viewPath", "marches/marches/marches");
				
				}
    		catch(Exception e){
    			// System.out.println(e);
				}
                // return "NO";

                 
        }
		
		return savedMarches.getCode();
		
        
    }
	
    
}





	
