package ne.mha.sinea.marches.marches;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Data;
import ne.mha.sinea.CommonProperties;

import ne.mha.sinea.nomenclature.typeMarches.TypeMarches;
import ne.mha.sinea.nomenclature.modePassation.ModePassation;
import ne.mha.sinea.nomenclature.financement.Financement;
import ne.mha.sinea.nomenclature.intervenant.Intervenant;

@Data
@Entity
@Table(name = "marches")
public class Marches extends CommonProperties {
    @Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "code")
	protected Integer code;
	@Column(name = "annee_marches_publique")
	protected Date anneeMarchesPublique;
	@Column(name = "objet")
	private String objet;
	@Column(name = "cout_previsionnel")
	protected Double coutPrevisionnel;
	@Column(name = "date_lancement_procedure")
	protected Date dateLancementProcedure;
	@Column(name = "date_attribution_contrat")
	protected Date dateAttributionContrat;
	@Column(name = "date_demarrage_prestation")
	protected Date dateDemarragePrestation;
	@Column(name = "date_achevement_prestation")
	protected Date dateAchevementPrestation;
	
	@JoinColumn(name = "code_intervenant", referencedColumnName = "code")
	@ManyToOne
	protected Intervenant intervenant;
	@JoinColumn(name = "code_type_marches", referencedColumnName = "code")
	@ManyToOne
	protected TypeMarches typeMarches;
	@JoinColumn(name = "code_financement", referencedColumnName = "code")
	@ManyToOne
	protected Financement financement;
	@JoinColumn(name = "code_mode_passation", referencedColumnName = "code")
	@ManyToOne
	protected ModePassation modePassation;
	


}
