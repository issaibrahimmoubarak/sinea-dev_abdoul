package ne.mha.sinea.marches.marches;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@NoArgsConstructor
@AllArgsConstructor


public class MarchesForm {
    private Date anneeMarchesPublique;
    private String objet;
	private Double coutPrevisionnel;
	private Date dateLancementProcedure;
	private Date dateAttributionContrat;
	private Date dateDemarragePrestation;
	private Date dateAchevementPrestation;
    private Integer codeIntervenant;
    private Integer codeTypeMarches;
    private Integer codeSourceFinancement;
    private Integer codeModePassation;
}
