package ne.mha.sinea.marches.marches;

import java.util.Date;
import java.util.List;
import org.springframework.data.repository.CrudRepository;

public interface MarchesRepository extends CrudRepository<Marches, Integer> {
    
    Marches findByCode(Integer code);
    Marches findByAnneeMarchesPublique(Date anneeMarchesPublique);
    List<Marches> findByIsDeletedFalse();
}
