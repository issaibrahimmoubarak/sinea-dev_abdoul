package ne.mha.sinea.marches.marches;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;
import ne.mha.sinea.Template;

import ne.mha.sinea.nomenclature.modePassation.ModePassation;
import ne.mha.sinea.nomenclature.modePassation.ModePassationRepository;
import ne.mha.sinea.nomenclature.typeMarches.TypeMarches;
import ne.mha.sinea.nomenclature.typeMarches.TypeMarchesRepository;
import ne.mha.sinea.nomenclature.financement.Financement;
import ne.mha.sinea.nomenclature.financement.FinancementRepository;
import ne.mha.sinea.nomenclature.intervenant.Intervenant;
import ne.mha.sinea.nomenclature.intervenant.IntervenantRepository;

@Controller
public class MarchesController {

    @Autowired
    MarchesRepository marchesService;
    @Autowired
    IntervenantRepository intervenantService;
    @Autowired
    ModePassationRepository modePassationeService;
    @Autowired
    FinancementRepository financementService;
    @Autowired
    TypeMarchesRepository typeMarchesService;

     //@PreAuthorize("hasAuthority('Ajout marches'))
	@GetMapping("/marches")
	public String  addMarches(MarchesForm marchesForm, Model model) {
		try{
			//récuperation de la liste des marches de la base de données et envoie de la liste récuperée à la vue à travers le modele
			List<Marches> marchess =  (List<Marches>) marchesService.findByIsDeletedFalse();
			model.addAttribute("marchess", marchess);
            List<Intervenant> intervenants =  (List<Intervenant>) intervenantService.findByIsDeletedFalse();
			model.addAttribute("intervenants", intervenants);
			List<ModePassation> modePassations =  (List<ModePassation>) modePassationeService.findByIsDeletedFalse();
			model.addAttribute("modePassations", modePassations);
			List<Financement> sourceFinancements =  (List<Financement>) financementService.findByIsDeletedFalse();
			model.addAttribute("sourceFinancements", sourceFinancements);
            List<TypeMarches> typeMarchess =  (List<TypeMarches>) typeMarchesService.findByIsDeletedFalse();
			model.addAttribute("typeMarchess", typeMarchess);
			
			
			model.addAttribute("horizontalMenu", "horizontalMenu");
			model.addAttribute("sidebarMenu", "configurationSidebarMenu");
			model.addAttribute("breadcrumb", "breadcrumb");
			model.addAttribute("navigationPath", "marches/marches");
			model.addAttribute("viewPath", "marches/marches/marches");
			
			}
		catch(Exception e){
				
			}
		//afficher le template avec la vue (formulaire) à l'intérieur
		return Template.defaultTemplate;

	}

    //validation du formulaire d'ajout marches
	@PostMapping("/marches")
    public String addMarchesSubmit(@Validated MarchesForm marchesForm,BindingResult bindingResult, Model model) {
    	//s'il n'ya pas d'erreur lors de la validation des données du formulaire
		if (!bindingResult.hasErrors()) {
			
			try {
				//récuperer les données saisies
				Marches PP = new Marches();
				
				PP.setAnneeMarchesPublique(marchesForm.getAnneeMarchesPublique());
				PP.setObjet(marchesForm.getObjet());
				PP.setCoutPrevisionnel(marchesForm.getCoutPrevisionnel());
				PP.setDateLancementProcedure(marchesForm.getDateLancementProcedure());
				PP.setDateAttributionContrat(marchesForm.getDateAttributionContrat());
				PP.setDateDemarragePrestation(marchesForm.getDateDemarragePrestation());
				PP.setDateAchevementPrestation(marchesForm.getDateAchevementPrestation());
				PP.setIntervenant(intervenantService.findByCode(marchesForm.getCodeIntervenant()));
				PP.setModePassation(modePassationeService.findByCode(marchesForm.getCodeModePassation()));
				PP.setFinancement(financementService.findByCode(marchesForm.getCodeSourceFinancement()));
				PP.setTypeMarches(typeMarchesService.findByCode(marchesForm.getCodeTypeMarches()));
				
				marchesService.save(PP);
				
					//récuperation de la liste des marches de la base de données et envoie de la liste récuperée à la vue à travers le modele
				List<Marches> marchess =  (List<Marches>) marchesService.findByIsDeletedFalse();
				model.addAttribute("marchess", marchess);
				List<Intervenant> intervenants =  (List<Intervenant>) intervenantService.findByIsDeletedFalse();
				model.addAttribute("intervenants", intervenants);
				List<ModePassation> modePassations =  (List<ModePassation>) modePassationeService.findByIsDeletedFalse();
				model.addAttribute("modePassations", modePassations);
				List<Financement> sourceFinancements =  (List<Financement>) financementService.findByIsDeletedFalse();
				model.addAttribute("sourceFinancements", sourceFinancements);
				List<TypeMarches> typeMarchess =  (List<TypeMarches>) typeMarchesService.findByIsDeletedFalse();
				model.addAttribute("typeMarchess", typeMarchess);
			
				//envoie de la liste récuperée à la vue à travers le modele
				model.addAttribute("marchess", marchess);
				//indiquer que l'operation d'ajout a réussie
				model.addAttribute("operationStatus", "operationStatus/success");
				model.addAttribute("horizontalMenu", "horizontalMenu");
				model.addAttribute("sidebarMenu", "configurationSidebarMenu");
				model.addAttribute("breadcrumb", "breadcrumb");
				// model.addAttribute("navigationPath", "marches/marchess");
				model.addAttribute("viewPath", "marches/marches/marches");
				
				}
			catch(Exception e){
					//récuperation de la liste des marches de la base de données et envoie de la liste récuperée à la vue à travers le modele
					List<Marches> marchess =  (List<Marches>) marchesService.findByIsDeletedFalse();
					model.addAttribute("marchess", marchess);
					List<Intervenant> intervenants =  (List<Intervenant>) intervenantService.findByIsDeletedFalse();
					model.addAttribute("intervenants", intervenants);
					List<ModePassation> modePassations =  (List<ModePassation>) modePassationeService.findByIsDeletedFalse();
					model.addAttribute("modePassations", modePassations);
					List<Financement> sourceFinancements =  (List<Financement>) financementService.findByIsDeletedFalse();
					model.addAttribute("sourceFinancements", sourceFinancements);
					List<TypeMarches> typeMarchess =  (List<TypeMarches>) typeMarchesService.findByIsDeletedFalse();
					model.addAttribute("typeMarchess", typeMarchess);
			
					model.addAttribute("operationStatus", "operationStatus/unsuccess");
					model.addAttribute("horizontalMenu", "horizontalMenu");
					model.addAttribute("sidebarMenu", "configurationSidebarMenu");
					model.addAttribute("breadcrumb", "breadcrumb");
					model.addAttribute("navigationPath", "marches/marches");
					model.addAttribute("viewPath", "marches/marches/marches");
					
					}
			
    	}
		else{
    		try{
    			
    			//récuperation de la liste des marches de la base de données et envoie de la liste récuperée à la vue à travers le modele
				List<Marches> marchess =  (List<Marches>) marchesService.findByIsDeletedFalse();
				model.addAttribute("marchess", marchess);
				List<Intervenant> intervenants =  (List<Intervenant>) intervenantService.findByIsDeletedFalse();
				model.addAttribute("intervenants", intervenants);
				List<ModePassation> modePassations =  (List<ModePassation>) modePassationeService.findByIsDeletedFalse();
				model.addAttribute("modePassations", modePassations);
				List<Financement> sourceFinancements =  (List<Financement>) financementService.findByIsDeletedFalse();
				model.addAttribute("sourceFinancements", sourceFinancements);
				List<TypeMarches> typeMarchess =  (List<TypeMarches>) typeMarchesService.findByIsDeletedFalse();
				model.addAttribute("typeMarchess", typeMarchess);
			
    			model.addAttribute("operationStatus", "operationStatus/unsuccess");	
				model.addAttribute("horizontalMenu", "horizontalMenu");
				model.addAttribute("sidebarMenu", "configurationSidebarMenu");
				model.addAttribute("breadcrumb", "breadcrumb");
				model.addAttribute("navigationPath", "marches/marches");
				model.addAttribute("viewPath", "marches/marches/marches");
				
				}
    		catch(Exception e){
				
				}
    	}
		
		//afficher le template avec la vue (formulaire) à l'intérieur
		return Template.defaultTemplate;
        
    }

	//modification d'un marches
	@GetMapping("/updateMarches/{code}")
    public String updateMarches(@PathVariable("code") int code,MarchesForm marchesForm, Model model) 
	{ 
		try
		{
			//récuperer les données saisies
			Marches PP = new Marches();
				
			PP.setAnneeMarchesPublique(marchesForm.getAnneeMarchesPublique());
			PP.setObjet(marchesForm.getObjet());
			PP.setCoutPrevisionnel(marchesForm.getCoutPrevisionnel());
			PP.setDateLancementProcedure(marchesForm.getDateLancementProcedure());
			PP.setDateAttributionContrat(marchesForm.getDateAttributionContrat());
			PP.setDateDemarragePrestation(marchesForm.getDateDemarragePrestation());
			PP.setDateAchevementPrestation(marchesForm.getDateAchevementPrestation());
			PP.setIntervenant(intervenantService.findByCode(marchesForm.getCodeIntervenant()));
			PP.setModePassation(modePassationeService.findByCode(marchesForm.getCodeModePassation()));
			PP.setFinancement(financementService.findByCode(marchesForm.getCodeSourceFinancement()));
			PP.setTypeMarches(typeMarchesService.findByCode(marchesForm.getCodeTypeMarches()));
			
			//récuperation de la liste des marches de la base de données et envoie de la liste récuperée à la vue à travers le modele
			List<Marches> marchess =  (List<Marches>) marchesService.findByIsDeletedFalse();
			model.addAttribute("marchess", marchess);
			List<Intervenant> intervenants =  (List<Intervenant>) intervenantService.findByIsDeletedFalse();
			model.addAttribute("intervenants", intervenants);
			List<ModePassation> modePassations =  (List<ModePassation>) modePassationeService.findByIsDeletedFalse();
			model.addAttribute("modePassations", modePassations);
			List<Financement> sourceFinancements =  (List<Financement>) financementService.findByIsDeletedFalse();
			model.addAttribute("sourceFinancements", sourceFinancements);
			List<TypeMarches> typeMarchess =  (List<TypeMarches>) typeMarchesService.findByIsDeletedFalse();
			model.addAttribute("typeMarchess", typeMarchess);
		
			
			model.addAttribute("horizontalMenu", "horizontalMenu");
			model.addAttribute("sidebarMenu", "configurationSidebarMenu");
			model.addAttribute("breadcrumb", "breadcrumb");
			model.addAttribute("navigationPath", "marches/marches");
			model.addAttribute("viewPath", "marches/marches/marches");
			
		}
		catch(Exception e)
		{
			
		}
		return Template.defaultTemplate;
    		
    }

	@PostMapping("/updateMarches/{code}")
    public RedirectView updateMarchesSubmit(@Validated MarchesForm marchesForm,BindingResult bindingResult,@PathVariable("code") int code, Model model,RedirectAttributes redirectAttributes) 
	{
		final RedirectView redirectView = new RedirectView("/marches", true);
    	if (!bindingResult.hasErrors()) 
    	{
			try
			{
				//récuperer les données saisies
			Marches PP = new Marches();
				
			PP.setAnneeMarchesPublique(marchesForm.getAnneeMarchesPublique());
			PP.setObjet(marchesForm.getObjet());
			PP.setCoutPrevisionnel(marchesForm.getCoutPrevisionnel());
			PP.setDateLancementProcedure(marchesForm.getDateLancementProcedure());
			PP.setDateAttributionContrat(marchesForm.getDateAttributionContrat());
			PP.setDateDemarragePrestation(marchesForm.getDateDemarragePrestation());
			PP.setDateAchevementPrestation(marchesForm.getDateAchevementPrestation());
			PP.setIntervenant(intervenantService.findByCode(marchesForm.getCodeIntervenant()));
			PP.setModePassation(modePassationeService.findByCode(marchesForm.getCodeModePassation()));
			PP.setFinancement(financementService.findByCode(marchesForm.getCodeSourceFinancement()));
			PP.setTypeMarches(typeMarchesService.findByCode(marchesForm.getCodeTypeMarches()));
		
				//indiquer que l'operation de modification a réussi
				redirectAttributes.addFlashAttribute("operationStatus", "operationStatus/success");
				
			}
			catch(Exception e)
			{
				//indiquer que l'operation de modification a échoué
				redirectAttributes.addFlashAttribute("operationStatus", "operationStatus/unsuccess");
			}
    	}
    	else
    	{
    		
    	}
		
    	//redirection vers la page d'ajout
		return redirectView;
        
    }
	
	@GetMapping("/deleteMarches/{code}")
	public RedirectView deleteMarches(@PathVariable("code") int code, Model model,RedirectAttributes redirectAttributes) 
	{ 
		final RedirectView redirectView = new RedirectView("/marches", true);
		try
		{
			Marches PP = marchesService.findByCode(code);
			PP.setIsDeleted(true);
			marchesService.save(PP);
			
			//indiquer que l'operation de suppression a réussi
			redirectAttributes.addFlashAttribute("operationStatus", "operationStatus/success");
		}
		catch(Exception e)
		{
			//indiquer que l'operation de suppression a échoué
			redirectAttributes.addFlashAttribute("operationStatus", "operationStatus/unsuccess");
		}
		
		//redirection vers la page d'ajout
		return redirectView;
	    		
	}

	
}
