package ne.mha.sinea.pem;

//import java.sql.Date;
import java.util.Date;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;
import ne.mha.sinea.CommonProperties;
import ne.mha.sinea.nomenclature.etatOuvrage.EtatOuvrage;
import ne.mha.sinea.nomenclature.financement.Financement;
import ne.mha.sinea.nomenclature.propriete.Propriete;
import ne.mha.sinea.referentiel.projet.Projet;
import ne.mha.sinea.referentiel.zone.Localite;

@Data
@Entity
@Table(name = "pem")
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS)
public abstract class PEM extends CommonProperties  {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "code")
	protected Integer code;
	@Column(name = "numero_irh")
	protected String numeroIRH;
	@Column(name = "nom_pem")
	protected String nomPEM;
	@Column(name = "annee_realisation")
	protected Date anneeRealisation;
	@JoinColumn(name = "code_financement", referencedColumnName = "code")
	@ManyToOne
	protected Financement financement;
	@JoinColumn(name = "code_projet", referencedColumnName = "code")
	@ManyToOne
	protected Projet projet;
	@JoinColumn(name = "code_localite", referencedColumnName = "code")
	@ManyToOne
	protected Localite localite;
	@Column(name = "latitude")
	protected Double latitude;
	@Column(name = "longitude")
	protected Double longitude;
	
	@Column(name = "hauteur_margelle")
	protected Double hauteurMargelle;
	@Column(name = "autre_info_margelle")
	protected String autreInfoMargelle;
	
	@JoinColumn(name = "code_propriete", referencedColumnName = "code")
	@ManyToOne
	protected Propriete propriete;
	
	@JoinColumn(name = "code_etat_ouvrage", referencedColumnName = "code")
	@ManyToOne
	protected EtatOuvrage etatOuvrage;
	
	@Column(name = "renseignement_divers")
	protected String renseignementDivers;
	
	@OneToMany(mappedBy="pem")
	protected List<DescriptionGeologique> descriptionGeologiques = new ArrayList<>();
	
	@OneToMany(mappedBy="pem")
	protected List<Prestataire> prestataires = new ArrayList<>();
	
}
