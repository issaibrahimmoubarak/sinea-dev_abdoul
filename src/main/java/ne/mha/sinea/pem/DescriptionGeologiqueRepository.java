package ne.mha.sinea.pem;

import java.util.List;
import org.springframework.data.repository.CrudRepository;

public interface DescriptionGeologiqueRepository extends CrudRepository<DescriptionGeologique, DescriptionGeologiqueKey> {
	
	DescriptionGeologique findByDescriptionGeologiqueKey(DescriptionGeologiqueKey code);
	List<DescriptionGeologique> findByIsDeletedFalse();
	List<DescriptionGeologique> findByPem_CodeAndIsDeletedFalse(Integer code);
	
}