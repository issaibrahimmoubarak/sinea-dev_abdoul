package ne.mha.sinea.pem;

import java.util.List;
import org.springframework.data.repository.CrudRepository;

public interface ForageRepository extends CrudRepository<Forage, Integer> {
	
	Forage findByCode(Integer code);
	Forage findByNumeroIRH(String IRH);
	List<Forage> findByIsDeletedFalse();
	
}