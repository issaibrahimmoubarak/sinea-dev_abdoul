package ne.mha.sinea.pem;

import java.util.List;
import org.springframework.data.repository.CrudRepository;

public interface PuitsCimenteRepository extends CrudRepository<PuitsCimente, Integer> {
	
	PuitsCimente findByCode(Integer code);
	PuitsCimente findByNumeroIRH(String IRH);
	List<PuitsCimente> findByIsDeletedFalse();
	
}