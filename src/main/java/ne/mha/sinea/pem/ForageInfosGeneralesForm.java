package ne.mha.sinea.pem;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ForageInfosGeneralesForm {
	
	private String numeroIRH = null;
	private Date anneeRealisation = null;
	private Double latitude = null;
	private Double longitude = null;
	private Integer codeFinancement = null;
	private Integer codeProjet = null;
	private Integer codeLocalite = null;
	private boolean foragePositif = false;
	private Date dateDemarrageForation = null;
	private Date dateFinForation = null;
	private Double profondeurTotaleForee = null;
	private Double profondeurTotaleEquipee = null;
	private Double profondeurNiveauStatique = null;
	private Date dateMesure = null;
	private Double hauteurMargelle = null;
	private String autreInfoMargelle = null;
	private boolean developpementEffectue = false;
	private Double debitAuSoufflage = null;
	private Double essaiSimplifieDureePompage = null;
	private Double essaiSimplifieRabattementMesure = null;
	private Double essaiSimplifieDebitMaximumEstime = null;
	private Double essaiLongueDureeDureePompage = null;
	private Double essaiLongueDureeRabattementMesure = null;
	private Double essaiLongueDureeDebitMaximumEstime = null;
	private Integer codeTypeUsage = null;
	private Integer codePropriete = null;
	private Integer codeTypeAmenagement = null;
	private String renseignementDivers = null;
}
