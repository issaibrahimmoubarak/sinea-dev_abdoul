package ne.mha.sinea.permission;

import java.util.List;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Integer> {

	User findByLogin(String login);
	User findByCode(Integer code);
	List<User> findByIsDeletedFalse();
	List<User> findByIsDeletedTrue();
}
