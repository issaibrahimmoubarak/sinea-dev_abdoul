package ne.mha.sinea.evenement;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import ne.mha.sinea.permission.Privilege;
import ne.mha.sinea.permission.PrivilegeRepository;
import ne.mha.sinea.permission.Role;
import ne.mha.sinea.permission.RoleRepository;
import ne.mha.sinea.permission.User;
import ne.mha.sinea.permission.UserRepository;

@Component
public class SineaEventListener {

	@Autowired
    private UserRepository userService;
	@Autowired
    private RoleRepository roleService;
	@Autowired
    private PrivilegeRepository privilegeService;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@EventListener
	public void ApplicationStarted (ApplicationStartedEvent e) {
		//initialiser l'application avec un utilisateur admin s'il n'existe pas encore en base
		if(userService.findByLogin("admin") == null) {
			Role role = new Role();
			role.setLibelle("admin");
			
			ArrayList<Privilege> privileges = new ArrayList<Privilege>();
			Privilege privilege1 = new Privilege();
			privilege1.setLibelle("gestion des accès");
			privileges.add(privilege1);
			
			Privilege privilege2 = new Privilege();
			privilege2.setLibelle("gestion des utilisateurs");
			privileges.add(privilege2);
			
			role.setPrivileges(privileges);
			
			User user = new User();
			user.setLogin("admin");
			user.setPrenom("admin");
			user.setNom("");
			user.setMotPasse(bCryptPasswordEncoder.encode("admin"));
			user.setActive(true);
			user.setTokenExpire(false);
			ArrayList<Role> roles = new ArrayList<Role>();
			roles.add(role);
			user.setRoles(roles);
	        userService.save(user);
		}
		
	}

}
