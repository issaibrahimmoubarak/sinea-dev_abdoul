package ne.mha.sinea.stationMeteo;

import java.io.Serializable;
import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ne.mha.sinea.nomenclature.parametreStationMeteo.ParametreStationMeteo;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ChroniqueObservationKey implements Serializable {

	private Station station;
	
	private ParametreStationMeteo parametreObserve;
	
	private Date dateDebutObservation;
	
	private Date dateFinObservation;
}
