package ne.mha.sinea;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

//controller par defaut
@Controller
public class SineaDefaultController {

	@GetMapping("/login")
	public String Connexion(Model model) {
		model.addAttribute("viewPath", "connexion");
		return "connexion";
	}

	@GetMapping("/")
	public String dashboard(Model model) {
		return "dashboard2";
	}

	@GetMapping("/test")
	public String test(Model model) {
		return "dashboard2";
	}

	@GetMapping("/publique")
	public String PointEau(Model model) {
		model.addAttribute("viewPath", "publique/home");
		return Template.publiqueLayout;
	}

	@GetMapping("/publique/stats/point_eau")
	public String pointEau(Model model) {
		model.addAttribute("viewPath", "publique/stats/point_eau");
		return Template.publiqueLayout;
	}

	@GetMapping("/publique/stats/hygienne_assainissement")
	public String hygieneAssainissenement(Model model) {
		model.addAttribute("viewPath", "publique/stats/hygienne_assainissement");
		return Template.publiqueLayout;
	}

	@GetMapping("/publique/programmation/p_ouvrages_hydrauliques")
	public String pOuvragesHydrauliques(Model model) {
		model.addAttribute("viewPath", "publique/programmation/p_ouvrages_hydrauliques");
		return Template.publiqueLayout;
	}
}
