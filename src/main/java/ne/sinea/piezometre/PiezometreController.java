package ne.sinea.piezometre;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ne.mha.sinea.Template;

@Controller
public class PiezometreController {

	@Autowired
	PiezometreRepository piezometreService;
	
	//@PreAuthorize("hasAuthority('gestion des unités')")
	@GetMapping("/Piezometre")
	public String  addPiezometre(PiezometreForm PiezometreForm, Model model) {
		try{
			List<Piezometre> Piezometres = piezometreService.findByIsDeletedFalseAndCodeInOrderByLibelleAsc(null);
			model.addAttribute("Piezometres", Piezometres);
			
			model.addAttribute("horizontalMenu", "horizontalMenu");
			model.addAttribute("sidebarMenu", "configurationSidebarMenu");
			model.addAttribute("breadcrumb", "breadcrumb");
			model.addAttribute("navigationPath", "Nomenclature/Piezometre");
			model.addAttribute("viewPath", "nomenclature/Piezometre/Piezometre");
			
			}
		catch(Exception e){
				
			}
		
		return Template.defaultTemplate;

	}
	
	
}
