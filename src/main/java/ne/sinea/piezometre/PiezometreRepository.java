package ne.sinea.piezometre;

import java.util.List;

import org.springframework.data.repository.CrudRepository;


public interface PiezometreRepository extends CrudRepository<Piezometre, Integer> {

	Piezometre findByCode(Integer code);

	Piezometre findByNom(String nom);

	List<Piezometre> findByIsDeletedFalse();

	List<Piezometre> findByIsDeletedTrue();

	List<Piezometre> findByIsDeletedFalseAndCodeInOrderByLibelleAsc(List<Integer> ListId);

}
